""" @file lab2a.py
    @brief This file contains a program which tests a user's reaction time.
    @package Lab0x02
    @brief This package contains lab2a.py, lab2b.py
    @author Neil Patel
    @date May 2, 2021
"""

import random, utime, pyb, micropython

def record_time():
    """ @brief Function that keeps track of time and displays the reaction
               time of the user on a button press.
        @details This program waits for a time interval between 2 and 3
                 seconds and then turns on the green LED on the Nucleo. It then
                 records the time difference between the lighting up of the
                 LED and the User button press (if any) and displays the
                 reaction time on the terminal. The green LED stays on for just
                 1 second.
        @return Returns nothing.
    """
    ## @brief Generates a random wait time between 2 - 3 seconds.
    wait_time = random.randrange(2, 3)
    utime.sleep(wait_time)
    ## @brief Flag that keeps track of whether a reaction was reported or not.
    reaction_flag = False
    blinko.high()
    ## @brief Keeps track of of response time.
    tick2 = 0
    ## @brief Keeps track of when LED turns on.
    tick1 = utime.ticks_us()
    while utime.ticks_us() < (tick1 + 1000000):
        if pinc13.value() == 0 and reaction_flag == False:
            tick2 = utime.ticks_us()
            diff = utime.ticks_diff(tick2, tick1)
            print("Your reaction time is: ", diff, "us")
            tries.append(diff)
            reaction_flag = True
    blinko.low()
    

if __name__ == "__main__":
    ## Initializes LED, interrupt on the User button, and code to run the
    ## program to test a user's reaction time.
    
    blinko = pyb.Pin(pyb.Pin.board.PA5, mode=pyb.Pin.OUT_PP)
    pinc13 = pyb.Pin (pyb.Pin.board.PC13, pyb.Pin.IN)
    
    ## @brief Keeps track of all the attempts.
    tries = []
    try:
        micropython.alloc_emergency_exception_buf(100)
        while True:
            record_time()
    except KeyboardInterrupt:
        if not tries:
            print("No reaction time successfully measured.")
        else:
            print("Your average reaction time is: ", (float)(sum(tries)/len(tries)), "us")
        