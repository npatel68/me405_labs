\hypertarget{lab0xff_sec_lab0xff_summary}{}\doxysection{Summary}\label{lab0xff_sec_lab0xff_summary}
The objective of this lab is to create a culminating term project that utilizes mechatronics and control skills by balancing a ball on top of our mechatronics kit. This project builds upon the system modelling and calculations done in HW0x02, HW0x04, and HW0x05. The project was broken into segments that dealt with assembling separate parts and task, and then integrating them to achieve functionality.

\hypertarget{lab0xff_sec_lab0xff_features}{}\doxysection{Design Features}\label{lab0xff_sec_lab0xff_features}
The individual parts that made up the system were the Touch Panel, Motors, and Encoders. The model of the platform was used to build the Controller, which relies on the ball\textquotesingle{}s position data (X,Y) from the touch panel and the motor angle data (theta\+\_\+X, theta\+\_\+Y) from the encoder, as well as their derivatives velocity and angular velocity. The final step to the project was to run all 4 segments together. This required a Scheduler to make sure all tasks ran cooperatively. Much of this was done through \mbox{\hyperlink{cotask_8py}{cotask.\+py}} which was provided by the instructor for this class. To easily share data (such as position and angle measurements), a second provided module called \mbox{\hyperlink{task__share_8py}{task\+\_\+share.\+py}} was used.\hypertarget{lab0xff_sec_lab0xff_touch_panel}{}\doxysection{Touch Panel}\label{lab0xff_sec_lab0xff_touch_panel}
The Touch Panel Sensor\textquotesingle{}s purpose is to determine the x and y position of the ball on the platform. ~\newline
 By controlling four pins (Two in the \char`\"{}\+X\char`\"{} direction and two in the \char`\"{}\+Y\char`\"{} direction), the distance in mm from the origin (the center of the panel) can be found. There are 3 primary functions to the Touch Panel Driver\+:
\begin{DoxyItemize}
\item scan\+\_\+\+X()\+: Returns the X coordinate in mm
\item scan\+\_\+\+Y()\+: Returns the Y coordinate in mm
\item scan\+\_\+\+Z()\+: Returns True if ball (or another object) is in contact with the panel, else returns False
\end{DoxyItemize}

The scan\+\_\+\+ALL() function can be used to conveniently perform all 3 measurements. More documentation can be found at \mbox{\hyperlink{touchPanel_8py}{touch\+Panel.\+py}}. ~\newline
 The Resistive Touch Panel works by having two wires that wind back and forth from on side to the other side of the panel. One wire\textquotesingle{}s resistance only increases by a noticeable amount in the X direction, while the other only in the Y direction. If you were to look at a point 2/3 of the way through the panel, then 2/3 of the resistance of the wire is before that point, and 1/3 after. When the touch panel is touched, the wires short together at that point. This allows the position of the touch to be measured. Each direction is measured individually by setting one side of the wire for that direction to 3.\+3 V, and the other to ground. The wire that is not being measured is connected to an ADC on one side while the other side is allowed to float. This allows the voltage at the intersection to be measured, which can be used to determine the position in that direction thanks to the linear increase of the wires resistance. To determine if something is touching the panel at all, one wire has one end connected to 3.\+3 V, while the other has one end connected to 0 V. If the other end of the 0 V connected wire is measured to also be 0 V, then the wires cannot be connected together, so the panel is not being touched. If the value read is nonzero, then the wires must have been connected and the panel was touched. ~\newline
 A simplified circuit diagram for the touch panel is shown below. The pins (Yp, Ym, Xp, Xm) in this case are configured for scan\+\_\+Z -\/ determining if the panel is being touched.



When the touch panel is not being touched, the two wires are not connected to each other, and in this case Xp is tied to 0 V while Ym is tied to 3.\+3 V. When the panel is touched at point P, the two wires intersect. Since Xp is no longer 0 V and Ym is no longer 3.\+3 V, we can determine that the touch panel is detecting something. To find the position itself, we just find the Voltage at point P -\/ the position in each direction will be the Voltage at point P as a percent of the input voltage multiplied by the total length of the panel in that direction. ~\newline
 

Below is an image of the touch panel on the platform, with nothing touching it. This was used to test the scan\+\_\+Z function in the benchmarks.



Below is an image of the touch panel on the platform, with the ball that it senses on top. This is the position used to measure the benchmark data.



Below is an image of the output of the functioning Touch\+Panel in Pu\+TTY, including the scan\+\_\+\+ALL and the total time for each operation.

\hypertarget{lab0xff_subsec_tp_calib}{}\doxysubsection{Calibration of Touch Panel}\label{lab0xff_subsec_tp_calib}
The Panel dimensions were found to be 192mm x 112mm. However, measurements did not always function on the border, so the possible measurement ranges are below\+:
\begin{DoxyItemize}
\item minimum X\+: -\/84 mm
\item maximum X\+: +84 mm
\item minimum Y\+: -\/48 mm
\item maximum Y\+: +42 mm
\end{DoxyItemize}

The minimum and maximum Y values likely differ since the panel is slightly off center in the Y direction -\/ there is an extra couple millimeters of dead space on the negative y side to give room for the wires used to connect it to the MCU. As a result, the measured center is slightly above the center of the panel, by about 3 mm. Otherwise, the center measurement was found to be spot on -\/ within a millimeter at least, though I lack the measurement tools to determine its exact position. However, measurements further out from the center became much less accurate, since the resistance of the panel did not seem to be linear.\hypertarget{lab0xff_subsec_tp_bench}{}\doxysubsection{Benchmarks for Timing}\label{lab0xff_subsec_tp_bench}
Each function was tested by running it 50 times in the benchmark. Timing was done using the ticks\+\_\+us function from the utime library.\hypertarget{lab0xff_sss_scanz}{}\doxysubsubsection{Scan\+\_\+\+Z Test\+:}\label{lab0xff_sss_scanz}
With the ball off of the platform\+:
\begin{DoxyItemize}
\item All 50 results were False, which is correct.
\item The Total benchmark time was 18392μs
\item The Average function time was {\bfseries{367.\+84μs}}
\end{DoxyItemize}

With the ball on the platform\+:
\begin{DoxyItemize}
\item All 50 results were True, which is correct.
\item The Total benchmark time was 18403μs
\item The Average function time was {\bfseries{368.\+06μs}}
\end{DoxyItemize}\hypertarget{lab0xff_sss_scany}{}\doxysubsubsection{Scan\+\_\+\+Y Test\+:}\label{lab0xff_sss_scany}
With the ball on the platform at around Y = -\/28mm\+:
\begin{DoxyItemize}
\item The maximum variation in Y was 0.\+073 mm
\item The Standard Deviation in Y was 0.\+02 mm
\item The Total benchmark time was 19712μs
\item The Average function time was {\bfseries{394.\+2μs}}
\end{DoxyItemize}\hypertarget{lab0xff_sss_scanx}{}\doxysubsubsection{Scan\+\_\+\+X Test\+:}\label{lab0xff_sss_scanx}
With the ball on the platform at around X = 20.\+7mm\+:
\begin{DoxyItemize}
\item The maximum variation in X was 0.\+17mm
\item The Standard Deviation in X was 0.\+038 mm
\item The Total benchmark time was 19568μs
\item The Average function time was {\bfseries{391.\+36μs}}
\end{DoxyItemize}\hypertarget{lab0xff_sss_scan_all}{}\doxysubsubsection{Scan\+\_\+\+ALL Test\+:}\label{lab0xff_sss_scan_all}
Testing all three functions together in the scan\+\_\+\+ALL() function\+:
\begin{DoxyItemize}
\item The Total benchmark time was 55139μs
\item The Average function time was {\bfseries{1102.\+78μs}}
\end{DoxyItemize}\hypertarget{lab0xff_sss_summary}{}\doxysubsubsection{Summary of Benchmarks}\label{lab0xff_sss_summary}
All three basic scan functions meet the required specs\+: they all measure under 500 microseconds individually. As a result the scan\+\_\+\+ALL() function can be performed in about 1.\+1 ms, which is easily faster than the estimated 1.\+5ms needed for the system to function smoothly. Both X and Y measurements were accurate across many trials, with only small variations. The X measurements varied more than the Y measurements most likely because the X axis is longer. The scan\+\_\+\+Z() function was equally consistent in identifying if the ball was in contact with the touch panel.\hypertarget{lab0xff_subsec_tp_usage}{}\doxysubsection{Usage of Touch Panel Driver}\label{lab0xff_subsec_tp_usage}
To use the touch panel with the driver, initialize an object for the class and assign the correct pins for each input. For example, if yp from the panel is connected to the PA6 pin on the MCU, then use yp=pyb.\+Pin.\+board.\+PA6 when initializing. Then, scan\+\_\+\+Z() can be used to check if something is touching the board before finding the position by calling scan\+\_\+\+X() and scan\+\_\+\+Y(), and then scan\+\_\+\+ALL() can be used to conveniently check all three -\/ it will return a tuple with (Z, X, Y).\hypertarget{lab0xff_sec_lab0xff_motors}{}\doxysection{Motors}\label{lab0xff_sec_lab0xff_motors}
The Motor Driver allows the DC motors to be driven at different power levels using PWM. ~\newline
 The user can specify the Duty cycle for each motor. If a fault occurs (such as when something is blocking the motor) the motor will automatically be disabled. It can be re-\/enabled with clear\+\_\+fault(). The motors can also be enabled or disabled with enable() and disable() respectively. Upon initializing the driver, takes the pins of the motor as well as the fault and sleep pins. Documentation can be found at \mbox{\hyperlink{motor_8py}{motor.\+py}}. ~\newline
 Below is an image of the two motors. To set dutycycle, use set\+\_\+level\+\_\+1() for Motor 1 and set\+\_\+level\+\_\+2() for motor 2. Duty cycle is used as a percent not a decimal.



Each motor has 2 inputs to control direction and torque. The two motors share the input n\+SLEEP, which tells the motors when not to sleep. They also both share the fault pin, n\+FAULT, which goes low whenever a fault occurs in one of the mots and must be cleared before operation may continue. The affect of the inputs on motor operation is shown below.



The motors spin when one input is high and the other low. With constant input voltages, the motors behave in an on or off manner In order to adjust the power of the motor, Pulse Width Modulation (PWM) was used. This allowed the dutycycle of the input wave to be adjusted. At 100\% duty cycle, when the wave is just a dc constant, the motors operate at full power. By decreasing the percent of time the wave is high compared to low, the power delivered to the motors can be decreased. The motor pins and their corresponding pins on the board are shown below. IN1/\+IN2 correspond to Motor 1, while IN3/\+IN4 correspond to Motor 2. The inputs are connected to a Timer for PWM.

\hypertarget{lab0xff_subsec_mot_video}{}\doxysubsection{Video Demonstration}\label{lab0xff_subsec_mot_video}
Below is a demonstration of motor 1 in action. It is being started at 0\% duty cycle and the duty cycle is increased by 1\% every half second, until it reaches a duty cycle of 100\%. At that point a fault is triggered by forcefully grabbing onto the motor, and the motor stops. (Note\+: the motor is grabbed before it reaches 100\%, so it does not trigger a fault at first).

\hypertarget{lab0xff_subsec_mot_benchmark}{}\doxysubsection{Dutycycle Benchmark}\label{lab0xff_subsec_mot_benchmark}
To test the minimum duty cycle needed for the motors to overcome static friction and begin spinning, each one was set to 0\% duty cycle, and then duty cycle was increased in intervals of 1\% until the motor began moving consistently. ~\newline
 For Motor 1\+:
\begin{DoxyItemize}
\item At $\sim$20\% dutycycle, motor begins inconsistent, stop and go movement.
\item Direction Counterclockwise\+: 27\% dutycycle needed
\item Direction Clockwise\+: 28\% dutycycle needed
\end{DoxyItemize}

For Motor 2\+:
\begin{DoxyItemize}
\item At $\sim$28\% dutycycle, motor begins inconsistent, stop and go movement.
\item Direction Counterclockwise\+: 33\% dutycycle needed
\item Direction Clockwise\+: 34\% dutycycle needed
\end{DoxyItemize}\hypertarget{lab0xff_sec_lab0xff_encoders}{}\doxysection{Encoders}\label{lab0xff_sec_lab0xff_encoders}
The Encoder Driver is used with the encoders to update the angle / angular velocity of the motors. ~\newline
 Like with the motors it initializes with the pins that connect to the encoders. The Encoder Driver class includes a method for updating the current position of the motor with update(), and a separate method for retrieving the current position (get\+\_\+position()). There is also a method for the change in angle from the previous update (the angular velocity) and one to manually reset the current angle to a chosen value. Documentation can be found at \mbox{\hyperlink{encoder_8py}{encoder.\+py}}. ~\newline
 The encoder driver automatically handles overflow (or underflow) by noting that if the tick count for the encoder has changed by over half its possible values, then it most likely has reached overflow, and then compensating by adding or subtracting 65535 (the total number of ticks in the encoder measurement). ~\newline
 To test that the encoders were working, the platform was tilted back and forth. The tick counter adjusted with the platform and increased in one direction but decreased in the other. From the starting position, it was possible to reach a negative position which indicated that the underflow was working correctly. Since overflow works similarly but in the opposite direction, it was assumed that overflow would also work fine. The video below demonstrates the usage of one of the encoders while tilting the platform. The data can be seen listed in the terminal -\/ the units are ticks, and there are around 4000 ticks to a rotation on the encoder. Apologies for the vertical format.



The Encoder pins are attached as described in the table. The pins they are connected to are connected totimers which support encoder functionality. These are necessary since the timing for encoders is too sensitive to do using pure software.

\hypertarget{lab0xff_sec_lab0xff_controller}{}\doxysection{Controller}\label{lab0xff_sec_lab0xff_controller}
The Controller object uses the input data of position, theta, velocity, and angular velocity to determine the correct duty cycle for the motor according to the system model. ~\newline
 When instantiating, it takes the K constants from HW0x05, \mbox{[}k1 k2 k3 k4\mbox{]} as arguments. The controller needs to be defined in both the x and y direction -\/ each controller takes one set of inputs and controls one motor. The only method in this class is Duty\+Cycle(x, theta, x\+\_\+dot, theta\+\_\+dot), which returns the correct duty cycle as a percent for the motors. More on usage can be found at \mbox{\hyperlink{controller_8py}{controller.\+py}}. ~\newline
 The Controller calculates Torque with T = -\/x$\ast$k, or T = (k1$\ast$x + k2$\ast$theta + k3$\ast$velocity + k4$\ast$ang\+\_\+velocity). Using the Torque, it finds the duty cycle using the Torque Constant K of the motors (13.\+8 m\+Nm/A) and the DC resistance (2.\+21 Ohms). The Duty cycle was found using the equation\+:





Where K\textquotesingle{} is the vector of K values described above and found in HW0x05, and the X vector contains the input parameters position, angle, velocity, and angular velocity. The constants in from HW0x05 were adjusted quite a bit to fit the system in the real world. This is likely due to a variety of factors including\+: unideal motors, inaccuracy in angle and position measurement, small timing errors leading to small differences in velocity measurements, friction, and other factors not taken into account in the theoretical system model.\hypertarget{lab0xff_sec_lab0xff_scheduler}{}\doxysection{Scheduler}\label{lab0xff_sec_lab0xff_scheduler}
The Scheduler, \mbox{\hyperlink{main_8py}{main.\+py}}, defines tasks for each of the above segments (Touch\+Panel, Motors, Encoders, and the Controller). ~\newline
 The Encoder and Touch Panel tasks are fairly straight forward -\/ they update position and angle respectively, and then save it to a shared queue so that the other tasks may access it. The shared queue is provided by \mbox{\hyperlink{task__share_8py}{task\+\_\+share.\+py}}. The Controller Task creates two instances of the Controller class using specified K values, one for each direction, and then uses the data from the Touch Panel and Encoders to get a duty cycle, which it also adds to a shared queue. The Motor Task then runs and sets the motors to the correct duty cycle. The tasks are set so that the Encoder and Touch Panel tasks run once every 40 ms, and the other two tasks run once every 15 ms. A list of scheduler tasks and shared queues and their descriptions is shown below.

\hypertarget{lab0xff_sec_lab0xff_std}{}\doxysection{State Transition Diagram}\label{lab0xff_sec_lab0xff_std}
The state transition diagram for the project is shown below.

\hypertarget{lab0xff_sec_lab0xff_vd}{}\doxysection{Video Demonstration}\label{lab0xff_sec_lab0xff_vd}
Neil\textquotesingle{}s working platform is demonstrated below. The video contains a brief explanation of our procedure\+:



Neil\textquotesingle{}s platform is a fair bit better at balancing the ball, and keeps steady while the ball is placed. It then moves the ball to a spot close to the center, though it does not get to the exact center. ~\newline
 Craig could not demo the project on his own hardware, as he accidentally broke his board (see Board Issues). This is the last recording of his still working platform\+:



While Craig did not have enough time to fully finish tuning his program, the platform clearly reacts to the touch and slowly moves back to equilibrium, though it somewhat overreacts due to its not optimal constant values for the controller.\hypertarget{lab0xff_sec_lab0xff_pd}{}\doxysection{Platform Data}\label{lab0xff_sec_lab0xff_pd}
Sample data taken during this process can be found in Documentation. Each .csv file has counts in the first column. To get to time, multiply by the period of Controller, or 15ms per entry. The second column includes the actual measurements. Data was not all from the same run. Data naturally varies from test to test, depending on ball placement, so this data does not reflect what would occur every time. The data of x and y positions are plotted below\+:



The X position of the ball started a little over halfway to the edge of the touch panel, at -\/40 mm. From there it is quickly balance to close to 0 mm, before over correction causes it to roll a bit further away. At time 1275 ms, the touch panel seems to have a small error and defaults values to 0. Luckily this is quickly corrected and the ball continues moving in the right direction.



Similar to the X data, the Touch panel gives a couple incorrect data measurements for the Y position around 1550 ms in. The 50mm measurement was probably caused by detecting the ball, but having the ball lose contact with the panel before sensing its location. As a result, it detected the ball in the furthest position. For the most part the ball started fairly close to the x axis here, so little correction was needed.\hypertarget{lab0xff_sec_lab0xff_mi}{}\doxysection{Major Issues}\label{lab0xff_sec_lab0xff_mi}
A fair few problems cropped up, especially when putting everything together\+:\hypertarget{lab0xff_subsec_tp_issues}{}\doxysubsection{Touch Panel Issues}\label{lab0xff_subsec_tp_issues}
The touch panel had a few problems. The most concerning was a flaky connection along one of the wires connecting it to the microcontroller board. This resulted in the panel occasionally losing connection and giving completely false data until the wire was readjusted. This error came and went, so was left unresolved since the panel still worked fine {\bfseries{most}} of the time. ~\newline
 Another major difficulty was the inaccuracy of the touch panel. The readings at either border were not always consistent -\/ while measurements were fairly accurate nearer to the center of the panel (after calibration at least), the non-\/linear resistance of the device made measurements less accurate the further out you went. The border was also not exactly at the edge of the panel -\/ touching the panel right on the corner gave nonsensical data. This problem gave problems for the controller, since it relied on accurate data to decide on a torque for the dc motors. Putting the ball too far from the platform made the measurements too inaccurate to work. ~\newline
 One last issue of note, which is fairly amusing looking back, was when a very small sticker somehow attached itself to the side of the panel. This resulted in the panel identifying a touch on that edge, and many, many hours were spent trying to determine the cause. This issue disappeared when the sticker was noticed and removed.\hypertarget{lab0xff_subsec_mot_issues}{}\doxysubsection{Motor Issues}\label{lab0xff_subsec_mot_issues}
The Motors worked fairly well, and PWM was relatively easy to implement. The biggest problem stemmed from the two motors having different torques at the same power. One motor was significantly weaker than the other. This was likely just due to a manufacturing difference, since they are the same model of motor. However, it created problems for the controller, as it needed to be adjusted so that one motor wasn\textquotesingle{}t running much harder than the other. ~\newline
 Another problem encountered with the motors was that the shaft was too loose for the platform arms, which led the motor to spin in place without moving the arms at all. There were three fixes for this problem -\/ tightening the screw on the platform arm,, super-\/gluing the the shaft to the arm, and somehow filling the space between the shaft and the arm. Tightening the arm was out of question since the heads of the screws were messed up, probably due to previous usage. The other two options were pretty terminal (if you chose them, it would take some time to undo and redo in case of any backtracking). Ultimately, Neil decided to put some tape on the motor shaft so that the arm-\/shaft connection is tighter and actually moves the platform.\hypertarget{lab0xff_subsec_enc_issues}{}\doxysubsection{Encoder Issues}\label{lab0xff_subsec_enc_issues}
The encoders worked quite well from the start, however, the original implementation only handled overflow and not underflow. This created some odd data before the issue was resolved. There was also a question of an issue with accuracy as the belt tying the motor and the encoder would sometimes slip, but this issue seemed negligible.\hypertarget{lab0xff_subsec_sched_issues}{}\doxysubsection{Scheduler Issues}\label{lab0xff_subsec_sched_issues}
The scheduler was easily the hardest part to get working, and as this is written, is still not fully operationsal -\/ it still lacks a proper UI. It does manage all the tasks well and they all run quickly and at the appropriate time. There were some issues with using too small of a period for the tasks, which caused them to be unable to complete on time. This was resolved by increasing the time each task would run for. The biggest problem with the scheduler was trying to get the correct K constant values. No matter how they were adjusted, they didn\textquotesingle{}t seem to work.\hypertarget{lab0xff_subsec_board_issues}{}\doxysubsection{Board Issues}\label{lab0xff_subsec_board_issues}
On Craig\textquotesingle{}s board, a screw came loose and fell onto the MCU board. This caused a short between two pins -\/ it even gave off a slight spark -\/ and the board stopped working. Even replacing the Nucleo board with a new one did not resolve the issue, and the voltage at various pins was incorrect -\/ so some damage must have been done to other components as well. This prevented Craig from getting the project running on his own hardware.\hypertarget{lab0xff_lab0xff_documentation}{}\doxysection{Documentation}\label{lab0xff_lab0xff_documentation}
Documentation of this lab can be found here\+: \mbox{\hyperlink{namespaceLab0xFF}{Lab0x\+FF}} ~\newline
 A copies of the CSV files can be found here\+: \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/collectedData.csv}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/collected\+Data.\+csv}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/xData.csv}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/x\+Data.\+csv}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/xDotData.csv}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/x\+Dot\+Data.\+csv}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/yData.csv}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/y\+Data.\+csv}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/yDotData.csv}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/y\+Dot\+Data.\+csv}}\hypertarget{lab0xff_lab0xff_source}{}\doxysection{Source Code}\label{lab0xff_lab0xff_source}
The source code for this lab\+: ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/controller.py}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/controller.\+py}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/encoder.py}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/encoder.\+py}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/lab4/main.py}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/lab4/main.\+py}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/motor.py}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/motor.\+py}} ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/labff/touchPanel.py}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/labff/touch\+Panel.\+py}}\hypertarget{lab0xff_lab0xff_repos}{}\doxysection{Repositories}\label{lab0xff_lab0xff_repos}
The repositories of the students involved in this project can be found at\+: ~\newline
 \href{https://bitbucket.org/npatel68/me405_labs/src/master/}{\texttt{ https\+://bitbucket.\+org/npatel68/me405\+\_\+labs/src/master/}} ~\newline
 \href{https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/}{\texttt{ https\+://bitbucket.\+org/cbelshe/me405\+\_\+craigbelshe/src/master/}} \DoxyHorRuler{0}
 \begin{DoxyAuthor}{Author}
Group 2\+: Neil Patel, Craig Belshe 
\end{DoxyAuthor}
\begin{DoxyCopyright}{Copyright}
Copyright © 2021 Neil Patel 
\end{DoxyCopyright}
\begin{DoxyDate}{Date}
June 11, 2021 
\end{DoxyDate}
