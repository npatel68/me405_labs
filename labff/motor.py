""" @file motor.py
    @brief A class providing methods to enable and control 2 DRV8847 motors with PWM
    @package Lab0xFF
    @brief This package contains touchPanel.py, motor.py, encoder.py, main.py, controller.py
    @author Group 2: Neil Patel, Craig Belshe
    @date June 11, 2021
"""

import pyb

class MotorDriver:
    
    def __init__ (self, nSLEEP_pin=pyb.Pin.board.PA15, nFAULT_pin=pyb.Pin.board.PB2,
                 INx_pin=pyb.Pin.board.PB5, OUTx_pin=pyb.Pin.board.PB4, 
                 INy_pin=pyb.Pin.board.PB1, OUTy_pin=pyb.Pin.board.PB0, 
                 INxy_timer=pyb.Timer(3), frequency=80000):
        '''!
        @brief  Initializes the pins for the motors, as well as the timer.

        @param[in] nSLEEP_pin   pyb.Pin, Pin for not sleep \n
            The default is pyb.Pin.board.PA15.
        @param[in] nFAULT_pin  pyb.Pin, Pin for not fault \n
            The default is pyb.Pin.board.PB2.
        @param[in] INx_pin     pyb.Pin, pin connected to positive on motor x \n
            The default is pyb.Pin.board.PB5.
        @param[in] OUTx_pin    pyb.Pin, pin connected to negative on motor x \n
            The default is pyb.Pin.board.PB4.
        @param[in] INy_pin     pyb.Pin, pin connected to positive on motor y \n
            The default is pyb.Pin.board.PB1.
        @param[in] OUTy_pin    pyb.Pin, pin connected to negative on motor y \n
            The default is pyb.Pin.board.PB0.
        @param[in] INxy_timer  pyb.Timer(), chooses which timer to use for PWM \n
            The default is pyb.Timer(3).
        @param[in] frequency   INT, defines the PWM frequency \n
            The default is 80000
        @return Returns nothing.

        '''
        # Motor starts off without faults so enabled is True
        self.enabled = True # Set to false when fault occurs
        
        # Define nsleep and nfault pins
        self.nsleep = nSLEEP_pin
        self.nfault = nFAULT_pin
        
        # Initialize nsleep and nfault as push-pull output pins
        self.nsleep.init(pyb.Pin.OUT_PP)
        self.nfault.init(pyb.Pin.OUT_PP)
        
        # Set interrupt to falling edge of nfault pin. 
        # Call the fault_CB method as a callback 
        pyb.ExtInt(pin=self.nfault, mode=pyb.ExtInt.IRQ_FALLING, 
                   pull=pyb.Pin.PULL_NONE, callback=self.fault_CB)
        
        # Define the Motor x and Motor y pins
        self.m1p = INx_pin
        self.m2p = INy_pin
        self.m1m = OUTx_pin
        self.m2m = OUTy_pin
        
        # Define the Timer and Frequency
        # Timer is centered so dutycycle is more precise
        self.tim = INxy_timer
        self.freq = frequency
        self.tim.init(freq=self.freq, mode=pyb.Timer.CENTER)
        
        # Initialize PWM channels on the timer for each motor pin
        # Motors are set off initially
        self.ch1 = self.tim.channel(1, pyb.Timer.PWM, pin=self.m1m, pulse_width_percent=0)
        self.ch2 = self.tim.channel(2, pyb.Timer.PWM, pin=self.m1p, pulse_width_percent=0)
        self.ch3 = self.tim.channel(3, pyb.Timer.PWM, pin=self.m2m, pulse_width_percent=0)
        self.ch4 = self.tim.channel(4, pyb.Timer.PWM, pin=self.m2p, pulse_width_percent=0)
    
    def enable(self):
        '''
        @brief Enables the motors
        @return Returns nothing
        '''
        # Set nsleep to true so motor is not sleeping -> motor is enabled
        self.nsleep.value(True)
    
    def disable(self):
        '''
        @brief Disables the motors
        @return Returns nothing
        '''
        # Set nsleep to false so motor is not not sleeping -> motor is disabled
        self.nsleep.value(False)
    
    def fault_CB(self):
        '''
        @brief A callback which disables the motors on fault detection
        @return Returns nothing
        '''
        # Sets enabled to false so the object knows not to turn motors on
        self.enabled = False
        
        # Turns off all motors
        self.ch1.pulse_width_percent(0)
        self.ch2.pulse_width_percent(0)
        self.ch3.pulse_width_percent(0)
        self.ch4.pulse_width_percent(0)
    
    def clear_fault(self):
        '''
        @brief Clears the fault so the motors can be used
        @return Returns nothing
        '''
        # Clear the fault so motors can be used again
        self.enabled = True
    
    def set_level_1(self, direction, level):
        '''
        @brief Spins motor 1 in direction at %level of its max power
        @param direction Boolean representing direcction of motor spin
        @param level Integer representing percentage level of motor power
        @return Returns nothing
        '''
        # Checks if a fault has occurred
        if self.enabled:
            
            # Checks direction
            # If direction is boolean True, spins clockwise
            # Level is specific to the motor charcteristics
            if direction:
                if level < 28:  # Ignore dutycycle below minimum needed to overcome static friction
                    level = 28
                self.ch1.pulse_width_percent(0)
                self.ch2.pulse_width_percent(level)
            else:
                if level < 27:
                    level = 27
                self.ch1.pulse_width_percent(level)
                self.ch2.pulse_width_percent(0)
        
    def set_level_2(self, direction, level):
        '''
        @brief Spins motor 2 in direction at %level of its max power
        @param direction Boolean representing direcction of motor spin
        @param level Integer representing percentage level of motor power        
        @return Returns nothing
        '''
        # Checks if a fault has occurred
        if self.enabled:
            # Checks direction
            # If direction is boolean True, spins clockwise
            # Level is specific to the motor charcteristics
            if direction:
                if level < 34:  # Ignore dutycycle below minimum needed to overcome static friction
                    level = 34
                self.ch3.pulse_width_percent(0)
                self.ch4.pulse_width_percent(level)
            else:
                if level < 33:
                    level = 33
                self.ch3.pulse_width_percent(level)
                self.ch4.pulse_width_percent(0)
    
if __name__ == "__main__":
    # For testing purposes 
     motor = MotorDriver()
     debug_count = 0
     while debug_count != 5000:     
         motor.enable()
         if debug_count < 2500:
             motor.set_level_1(0,50)
             motor.set_level_2(1,40)
         else:
             motor.set_level_1(1,50)
             motor.set_level_2(0,40)
         print(motor.enabled)
         debug_count += 1
     motor.disable()
    