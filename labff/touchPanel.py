""" @file touchPanel.py
    @brief Drivers for receiving data from the Resistive Touch Panel.
    @package Lab0xFF
    @brief This package contains touchPanel.py, motor.py, encoder.py, main.py, controller.py
    @author Group 2: Neil Patel, Craig Belshe
    @date June 11, 2021
"""

import pyb
import utime

class TouchDriver:
    '''
    @brief      A driver to allow for interaction with  Resistive touch panel
    @details    This class defines methods to interact with a touch panel - measuring the X and Y position of a touch,
                as well as determining if anything is touching the panel.
    '''
       
    def __init__(self, ym=pyb.Pin.board.PA0, xm=pyb.Pin.board.PA1, 
                 yp=pyb.Pin.board.PA6, xp=pyb.Pin.board.PA7):
        '''
        @brief Initializes the TouchPanel object with the correct pins and measurements.
        @param ym the pyb.pin for the minus y (y-) wire from the panel (default: pyb.Pin.board.PA0)
        @param xm the pyb.pin for the minus x (x-) wire from the panel (default: pyb.Pin.board.PA1)
        @param yp the pyb.pin for the plus y (y+) wire from the panel (default: pyb.Pin.board.PA6)
        @param xp the pyb.pin for the plus x (x+) wire from the panel (default: pyb.Pin.board.PA7)
        @returns Returns nothing.
        '''
        self.ym = ym
        self.xm = xm
        self.yp = yp
        self.xp = xp
        self.length = 100
        self.width = 176
    
    def scan_X(self):
        '''
        @brief      Scans the touch panel for the X value of the touch in mm relative to the center
        @returns    x value in mm
        '''
        # Set x pins to out
        self.xm.init(pyb.Pin.OUT_PP)
        self.xp.init(pyb.Pin.OUT_PP)
        
        self.xm.value(False) # xm is 0 V
        self.xp.value(True) # xp is 3.3 V
        
        # Set y pins to in
        adc = pyb.ADC(self.yp)
        self.ym.init(pyb.Pin.IN)
        
        #wait x amount of time
        #utime.sleep_us(150)
        
        # Measure yp
        Vx = adc.read()
        return ((Vx / 4095 * self.width) - self.width/2) # x-pos in mm
    
    def scan_Y(self):
        '''
        @brief      Scans the touch panel for the Y value of the touch in mm relative to the center
        @returns    y value in mm
        '''
        # Set y pins to out
        self.ym.init(pyb.Pin.OUT_PP)
        self.yp.init(pyb.Pin.OUT_PP)
        
        self.ym.value(False) # ym is 0 V
        self.yp.value(True) # yp is 3.3 V
        
        # Set x pins to in
        adc = pyb.ADC(self.xp)
        self.xm.init(pyb.Pin.ANALOG)
        
        #utime.sleep_us(150)
        
        # Measure xp
        Vy = adc.read()
        return -((Vy / 4095 * self.length) - self.length/2) # y-pos in mm
    
    def scan_Z(self):
        '''
        @brief      Scans the touch panel to check if it detects anything
        @returns    boolean, True if object is detected
        '''
        self.xm.init(pyb.Pin.OUT_PP)
        self.yp.init(pyb.Pin.OUT_PP)
        
        self.xm.value(False) # xm is 0 V
        self.yp.value(True) # yp is 3.3 V
        
        adc = pyb.ADC(self.xp) # read xm
        self.ym.init(pyb.Pin.ANALOG) # float ym
        
        #utime.sleep_us(150)
        
        if adc.read() < 10:
            return False
        
        return True
    
    def scan_ALL(self):
        '''
        @brief      Scans the touch panel for all measurements
        @returns    (Z,X,Y) Triple containing the x, y, and z values
        '''
        # Ticks used for testing time taken to read all channels
        # tick1 = utime.ticks_us()
        ## @brief Scanned Z value
        z = self.scan_Z()
        ## @brief Scanned X value
        x = self.scan_X()
        ## @brief Scanned Y value
        y = self.scan_Y()
        # tick2 = utime.ticks_us()
        # print("Time to read (us): ", utime.ticks_diff(tick2, tick1), "\n")
        return (z, x, y)

if __name__ == '__main__':
    # Test class
    panel = TouchDriver()
    while True:
        if panel.scan_Z():
            #print('x =', panel.scan_X(), '\ny =', panel.scan_Y(), '\n')
            print(panel.scan_ALL(), '\n')
            utime.sleep(1)
        utime.sleep_ms(10)
        
 