var searchData=
[
  ['task_96',['Task',['../classcotask_1_1Task.html',1,'cotask']]],
  ['task_5fenc_97',['task_enc',['../main_8py.html#a63b233af19a20c20b766224b90c4819f',1,'main']]],
  ['task_5flist_98',['task_list',['../cotask_8py.html#ae54e25f8d14958f642bcc22ddeb6c56f',1,'cotask']]],
  ['task_5fshare_2epy_99',['task_share.py',['../task__share_8py.html',1,'']]],
  ['tasklist_100',['TaskList',['../classcotask_1_1TaskList.html',1,'cotask']]],
  ['theta_5fx_5fdot_5fvals_101',['theta_x_dot_vals',['../main_8py.html#af89be64e259e709d7b89fd36ee26becd',1,'main']]],
  ['theta_5fx_5fvals_102',['theta_x_vals',['../main_8py.html#a1d9ff33e8d72f1b81d9d298e337c21ed',1,'main']]],
  ['theta_5fy_5fdot_5fvals_103',['theta_y_dot_vals',['../main_8py.html#a14d1f34f3b163361458adac402b3b1a4',1,'main']]],
  ['theta_5fy_5fvals_104',['theta_y_vals',['../main_8py.html#a7e00ef6c7a3cddcf3767f313e152de15',1,'main']]],
  ['time_105',['time',['../UI__front_8py.html#aa88408f92efb28993de95812284a0e34',1,'UI_front']]],
  ['timmy_106',['timmy',['../lab2b_8py.html#a6a7da999aefdca5b6c672c39de077d25',1,'lab2b']]],
  ['touch_5ftask_107',['touch_task',['../main_8py.html#a9dcb9d48e3ee22c4ff67ef482e1bfe04',1,'main']]],
  ['touchdriver_108',['TouchDriver',['../classtouchPanel_1_1TouchDriver.html',1,'touchPanel']]],
  ['touchpanel_2epy_109',['touchPanel.py',['../touchPanel_8py.html',1,'']]],
  ['tries_110',['tries',['../lab2a_8py.html#a0e52513e6647d3a8e3d440bb786207f2',1,'lab2a.tries()'],['../lab2b_8py.html#a75efbea126950aab12cec98fde20b2fa',1,'lab2b.tries()']]]
];
