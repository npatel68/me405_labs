var annotated_dup =
[
    [ "controller", null, [
      [ "Ctrl", "classcontroller_1_1Ctrl.html", "classcontroller_1_1Ctrl" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ]
    ] ],
    [ "encoder", null, [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ]
    ] ],
    [ "mcp9808", null, [
      [ "iic", "classmcp9808_1_1iic.html", "classmcp9808_1_1iic" ]
    ] ],
    [ "motor", null, [
      [ "MotorDriver", "classmotor_1_1MotorDriver.html", "classmotor_1_1MotorDriver" ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ]
    ] ],
    [ "touchPanel", null, [
      [ "TouchDriver", "classtouchPanel_1_1TouchDriver.html", "classtouchPanel_1_1TouchDriver" ]
    ] ]
];