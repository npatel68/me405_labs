var namespaces_dup =
[
    [ "adc", null, [
      [ "testADC", "adc_8py.html#ae8c6eca91d42c1b3323dd30cc9e8fc14", null ],
      [ "adc", "adc_8py.html#a26f07d0d11df2a0cf58cbae8ae0eac6c", null ],
      [ "buffy", "adc_8py.html#a39ab43aff1a75658228f8853b95328ed", null ],
      [ "pina0", "adc_8py.html#acb41a5fec6764e8e758d1c9719b53b4e", null ],
      [ "pinc13", "adc_8py.html#acc0291ec7cf8f3ae1e9c27f2ad39ca08", null ]
    ] ],
    [ "controller", null, [
      [ "Ctrl", "classcontroller_1_1Ctrl.html", "classcontroller_1_1Ctrl" ]
    ] ],
    [ "cotask", null, [
      [ "Task", "classcotask_1_1Task.html", "classcotask_1_1Task" ],
      [ "TaskList", "classcotask_1_1TaskList.html", "classcotask_1_1TaskList" ],
      [ "task_list", "cotask_8py.html#ae54e25f8d14958f642bcc22ddeb6c56f", null ]
    ] ],
    [ "encoder", null, [
      [ "EncoderDriver", "classencoder_1_1EncoderDriver.html", "classencoder_1_1EncoderDriver" ],
      [ "enc", "encoder_8py.html#af315a97816f16a1e22e2403e35d47362", null ]
    ] ],
    [ "intTemp", null, [
      [ "adc", "intTemp_8py.html#acd878249293e06e2b6534ba2919b4070", null ],
      [ "i", "intTemp_8py.html#a8286580d2f21ecd01c22d634aab410d4", null ],
      [ "val", "intTemp_8py.html#a3c8c8105cc302e0cf7dce555a19e4778", null ]
    ] ],
    [ "Lab0x01", "namespaceLab0x01.html", null ],
    [ "Lab0x02", "namespaceLab0x02.html", null ],
    [ "Lab0x03", "namespaceLab0x03.html", null ],
    [ "Lab0x04", "namespaceLab0x04.html", null ],
    [ "Lab0xFF", "namespaceLab0xFF.html", null ],
    [ "lab2a", null, [
      [ "record_time", "lab2a_8py.html#a807569abb08ba2ec8a35492f2f57ef25", null ],
      [ "blinko", "lab2a_8py.html#ac35968841de386035fbeb49f88209d6d", null ],
      [ "pinc13", "lab2a_8py.html#abc996688dd1149ec316feeaf52b6028f", null ],
      [ "tries", "lab2a_8py.html#a0e52513e6647d3a8e3d440bb786207f2", null ]
    ] ],
    [ "lab2b", null, [
      [ "cb1", "lab2b_8py.html#a97ad1e4fbbac7a8d6d95ed0fb871d6b4", null ],
      [ "cb2", "lab2b_8py.html#a42a75dd4fb564ee78eda4c87f0ec4a3d", null ],
      [ "ch1", "lab2b_8py.html#a08d48779e326982639a78e5e4aa5420c", null ],
      [ "ch2", "lab2b_8py.html#af507ff31a83e5c09597b0f12d4610b29", null ],
      [ "comp", "lab2b_8py.html#a57c1b7fc84742ac1f446201189ecfd98", null ],
      [ "count", "lab2b_8py.html#adff53c8d4117aff4fbd01d10ff38f8d0", null ],
      [ "pina5", "lab2b_8py.html#a6c5c8e06719139c3f293db696dbe2770", null ],
      [ "pinb3", "lab2b_8py.html#a5cae82f0f114d3aba2d789635e8c0e2f", null ],
      [ "pinc13", "lab2b_8py.html#ab6f4be640dfe20a77e5f0edac3e99b0c", null ],
      [ "rTime", "lab2b_8py.html#a536dc98897ed4c3dccedbda3c0428b2d", null ],
      [ "timmy", "lab2b_8py.html#a6a7da999aefdca5b6c672c39de077d25", null ],
      [ "tries", "lab2b_8py.html#a75efbea126950aab12cec98fde20b2fa", null ]
    ] ],
    [ "main", null, [
      [ "ctrl_task", "main_8py.html#a1d9e05dbc69d1e40e192647aa2178521", null ],
      [ "data_task", "main_8py.html#a8a74c41fb93f5502bbb54222cc590304", null ],
      [ "encode_task", "main_8py.html#ad05721f31d28395d9bf3c3456333c214", null ],
      [ "motor_task", "main_8py.html#a22c02d46e13fef03586e7261503784e6", null ],
      [ "touch_task", "main_8py.html#a9dcb9d48e3ee22c4ff67ef482e1bfe04", null ],
      [ "UI_task", "main_8py.html#a4c99f2c8eaadf389b66063ab1acdc5d3", null ],
      [ "debug_count", "main_8py.html#a3d18e3b48a5df7effd4acee5c47b454d", null ],
      [ "queue_ang", "main_8py.html#a83dc4dbeb630401fb69d55423570c234", null ],
      [ "queue_end", "main_8py.html#a1c2989f51ccf99f8ac5c6bbae29a2d48", null ],
      [ "queue_motor1", "main_8py.html#adf358b481d64839594cddd7e16cd80fc", null ],
      [ "queue_motor2", "main_8py.html#a51ebc4e2267a644b5cf939ec5e533de9", null ],
      [ "queue_vang", "main_8py.html#acf4de1ea89e1fe020a55265e1d3f7bb1", null ],
      [ "queue_x", "main_8py.html#a099083add37190ff0e80dc4ccb578009", null ],
      [ "queue_y", "main_8py.html#ae8a91ddbf2d97b154e4d74b3ddcb4caa", null ],
      [ "task_ctrl", "main_8py.html#ae1c78a5a9024ab1aff1291029b2f7c80", null ],
      [ "task_enc", "main_8py.html#a63b233af19a20c20b766224b90c4819f", null ],
      [ "task_mot", "main_8py.html#a228b43336567159754805fe96b407cb3", null ],
      [ "task_tp", "main_8py.html#ae5b0ca497789c4d8d580c6462e2db703", null ],
      [ "theta_x_dot_vals", "main_8py.html#af89be64e259e709d7b89fd36ee26becd", null ],
      [ "theta_x_vals", "main_8py.html#a1d9ff33e8d72f1b81d9d298e337c21ed", null ],
      [ "theta_y_dot_vals", "main_8py.html#a14d1f34f3b163361458adac402b3b1a4", null ],
      [ "theta_y_vals", "main_8py.html#a7e00ef6c7a3cddcf3767f313e152de15", null ],
      [ "x_dot_vals", "main_8py.html#a7bf2e29f8ffcad0fa665dfcd54383396", null ],
      [ "x_vals", "main_8py.html#add7b621500f261e27b002da11e1631a9", null ],
      [ "y_dot_vals", "main_8py.html#a80a23d55466cf24973d616b01f0706e4", null ],
      [ "y_vals", "main_8py.html#aa83c2d887e85d1ab70800ee2cbf8be7a", null ]
    ] ],
    [ "mcp9808", null, [
      [ "iic", "classmcp9808_1_1iic.html", "classmcp9808_1_1iic" ],
      [ "baudrate", "mcp9808_8py.html#ac9867d0c323a356515d552b55159e6e5", null ],
      [ "eye2see", "mcp9808_8py.html#ac5462e8415c1d9817996ca80403a1f13", null ],
      [ "i2c", "mcp9808_8py.html#ae99e487519ec535e9356de31b7f1dee7", null ],
      [ "MASTER", "mcp9808_8py.html#a836d2d0f8fafb6bd9bb05908da0b0a29", null ]
    ] ],
    [ "motor", null, [
      [ "MotorDriver", "classmotor_1_1MotorDriver.html", "classmotor_1_1MotorDriver" ],
      [ "debug_count", "motor_8py.html#a149669eee4b87974e01e56c9096db15e", null ],
      [ "motor", "motor_8py.html#a0f0b613e19496b93e35812bf585d8c2f", null ]
    ] ],
    [ "task_share", null, [
      [ "Queue", "classtask__share_1_1Queue.html", "classtask__share_1_1Queue" ],
      [ "Share", "classtask__share_1_1Share.html", "classtask__share_1_1Share" ],
      [ "show_all", "task__share_8py.html#a130cad0bc96d3138e77344ea85586b7c", null ],
      [ "share_list", "task__share_8py.html#a75818e5b662453e3723d0f234c85e519", null ]
    ] ],
    [ "touchPanel", null, [
      [ "TouchDriver", "classtouchPanel_1_1TouchDriver.html", "classtouchPanel_1_1TouchDriver" ],
      [ "panel", "touchPanel_8py.html#ad1ecc8f62b7a3c61109318c2ce9b8b08", null ]
    ] ],
    [ "UI_front", null, [
      [ "kb_cb", "UI__front_8py.html#aa21ab56e8f0029f944c615900213ebdd", null ],
      [ "sendVals", "UI__front_8py.html#a91eed7dff1ab057279b1dd00b5b3959d", null ],
      [ "data", "UI__front_8py.html#a6fd3153bbe3baed376cd2dcdad579244", null ],
      [ "file1", "UI__front_8py.html#a1de879849ea08fd082f0c72f45118801", null ],
      [ "last_key", "UI__front_8py.html#ab06f43189082ba23a4383d5d65049704", null ],
      [ "line", "UI__front_8py.html#a11e27c46d3bd2c49d02df2423d3d7bca", null ],
      [ "ser", "UI__front_8py.html#a26d65cb7ea7d47e295007598eedfb6ba", null ],
      [ "time", "UI__front_8py.html#aa88408f92efb28993de95812284a0e34", null ]
    ] ],
    [ "vendo", null, [
      [ "kb_cb", "vendo_8py.html#a92b1e3a4a1d1398889f106f18cf46c6e", null ],
      [ "VendotronTask", "vendo_8py.html#a9fed49d43101743f5a8240f570f8727c", null ],
      [ "last_key", "vendo_8py.html#a10853866c82014f11d5621e87b52616e", null ],
      [ "vendo", "vendo_8py.html#afc7c9223cbbe0c449c296a7b3c5fbb99", null ]
    ] ]
];