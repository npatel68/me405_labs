""" @file controller.py
    @brief A class driver for controlling the motors to give the appropriate Torque.
    @package Lab0xFF
    @brief This package contains touchPanel.py, motor.py, encoder.py, main.py, controller.py
    @author Group 2: Neil Patel, Craig Belshe
    @date June 11, 2021
"""

class Ctrl:
    def __init__(self, k1, k2, k3, k4):
        '''
        @brief  Initializes the Controller based on calculated gain values
        @param k1, k2, k3, k4   The constants calculated for the system model
        @return Returns nothing.
        '''

        self.k1 = k1
        self.k2 = k2
        self.k3 = k3
        self.k4 = k4
        
    def DutyCycle(self, pos, ang, vel, ang_vel):
        '''
        @brief          Determines the correct duty cycle for the motor to follow the system model
        @param pos      The position of the ball according to the touch panel measurement
        @param ang      The angle of the ball according to the encoder measurement
        @param vel      The velocity / change in position of the ball
        @param ang_vel  The angular velocity / change in angle of the ball
        @return Returns the calculated Duty Cycle for the appropriate Torque.
        '''

        ## @brief Calculated Tx = -k*x
        T = self.k1*pos + self.k2*ang + self.k3*vel + self.k4*ang_vel
        ## @brief 2.21 Ohms DC Resistance for the motor
        R = 2.21    
        ## @brief 13.8 mNm/A Torque Constant
        KT = 0.0138
        ## @brief 12 V DC motor voltage
        Vdc = 12
        
        # Return the calculated Dutycycle for the appropriate Torque
        return 100*(R/(4*KT*Vdc))*T

