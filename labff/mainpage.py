## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website serves as documentation for code developed by Neil Patel in 
#  ME405: Mechatronics as well as his portfolio for the class. See individual
#  modules for details.\n\n
#  \b Modules:
#  - @ref hw0x02
#  - @ref hw0x04
#  - @ref hw0x05
#  - @ref lab0x01
#  - @ref lab0x02
#  - @ref lab0x03
#  - @ref lab0x04
#  - @ref lab0xff
#
#  @section repo Source Code Repository
#  The repository containing all the source code is:
#  https://bitbucket.org/npatel68/me405_labs/src/master/ \n
#  The repository of my Term Project partner:
#  https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/ \n
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date June 11, 2021
#
#  @page hw0x02 HW0x02: Term Project System Modeling
#  @image html HW0x02_pg_1.png "Page 1" width=50% height=50%
#  @image html HW0x02_pg_2.png "Page 2" width=50% height=50%
#  @image html HW0x02_pg_3.png "Page 3" width=50% height=50%
#  @image html HW0x02_pg_4.png "Page 4" width=50% height=50%
#  @image html HW0x02_pg_5.png "Page 5" width=50% height=50%
#  @image html HW0x02_pg_6.png "Page 6" width=50% height=50%
#  - - -
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @copyright Copyright © 2021 Neil Patel
#  @date April 30, 2021
#
#  @page hw0x04 HW0x04: Simulation or Reality?
#  @section sec_hw0x04_overview Overview
#  The goal of this assignment is to model the Equations of Motion derived in 
#  HW0x02 as a Linear State Space in the form of x_dot = Ax + Bu, where x is 
#  the state vector, A is the coefficient Matrix to the state vector, u is the 
#  input vector, B is the coeffcient Matrix to the input vector, and x_dot is 
#  the time derivative of the state vector x. In the context of the Equations 
#  of Motion representing the Ball-Plate system derived in Homework 2, the 
#  state vector would be a row vector of the position x, angle theta y and their 
#  derivatives, while the input vector is the torque output from the motor, and 
#  x_dot is the acceleration of the ball and the angular acceleration of the 
#  platform about the y axis. The implementation of this state space involved 
#  getting a matrix G that represented the matrix of accelerations from the 
#  inverse of the Mass Matrix M times the Forcing Function Matrix f. From there, 
#  the G matix was appended to a matrix h with x_dot (velocity) and theta_dot 
#  (angular velocity) as the first two elements in that matrix. The model was 
#  then linearized using Jacobian Matricies, which yielded A and B. From there, 
#  the Matricies A and B were implemented into a MatLab Simulink Model (see below) 
#  that calculated the output (position, angle, velocity and angular velocity) 
#  for both Open and Closed-loop controller set ups. Note the Gain for open loop 
#  is given as K = [0 0 0 0], and the Gain for closed loop was approximated to 
#  be K = [-0.3 -0.2 -0.05 -0.02] in the interest of evaluating system performance. 
#  Three test cases were used to verify the model's accuracy:
#
#  - Open Loop, with all 0 initial conditions, running for 1 second.
#  - Open Loop, with the only non-zero initial condition being x = 5 cm, running 
#  for 0.4 seconds.
#  - Closed Loop, with the same initial conditions as Case 2 except T_x = -Kx, 
#  running for 20 seconds.\n\n
#
#  @image html simLink.png "The Simulink Model used to generate output." width=50% height=50%
#
#  @section sec_hw0x04_case1 Case 1: Open Loop with Zero Initial Conditions
#
#  @image html case1_1.png width=50% height=50%
#  @image html case1_2.png width=50% height=50%
#
#  As one might expect from a system with zero initial conditions, there is no 
#  response in any element of the state vector. This acts as a sort of first 
#  "sanity check" that ensures no zero error occurs with no input in the model.
#
#  @section sec_hw0x04_case2 Case 2: Open Loop with x_i = 5 cm
#
#  @image html case2_1.png width=50% height=50%
#  @image html case2_2.png width=50% height=50%
#
#  This acts as a second sanity check, where the ball starts 5 cm off the center 
#  of gravity of the platform with no torque input from the motor. The results 
#  are in agreement with what one would expect - that the platform begins to 
#  tilt due to the moment caused by the weight of the ball about the platform's 
#  center of rotation. The ball is shown to move towards the center a bit before 
#  rolling to the edge of the platform at an increasing velocity as the angle of 
#  platform tilt increases. With no torque input to correct this, the ball will 
#  simply roll off the edge at some time t in the future. Moreover, the ball moves 
#  barely more than a centimeter and reaches a velocity of just over 0.2 m/s, 
#  which makes sense given the short time frame and beginnng from rest - this 
#  validates the model for open loop responses from an intuition point of view.
#
#  @section sec_hw0x04_case3 Case 3: Closed Loop with x_i = 5 cm and T_x = -Kx
#
#  @image html case3_1.png width=50% height=50%
#  @image html case3_2.png width=50% height=50%
#
#  Finally, we have a closed control loop with torque input from the motor and 
#  constant gain values. Note the gain values used are by no means ideal, which 
#  is why the system response takes roughly 15 to 20 seconds to reach steady state 
#  that is, the ball is balanced in the center of the platform. Otherwise, the 
#  output on the plots mostly agree with what makes sense with intuition. The 
#  ball oscillates from side to side on the platform as the platform moves to 
#  decrease the velocity of the ball and bring it back to a neutral position. 
#  As expected, the plots show a gradual decay in the magnitude of all state 
#  variables until they reach a steady state as the ball approaches the center 
#  of platform. Additionally, note the magnitude of the numbers makes sense, 
#  since the ball's position always decreases from 5 cm, while the platform's 
#  angle initially increases with every peak to compensate for the ball's rolling, 
#  but it eventually decreases as the ball's rolling becomes more controlled. 
#  Additionally, the ball's velocity never grows much beyond 0.1 m/s and the 
#  platform's angular velocity never gets larger than 0.2 seconds. These low 
#  numbers make sense for a system this small in scale, which validates the 
#  model's efficacy for Closed Loop Control. 
#
#  @section hw0x04_source Source Code 
#  For the live Matlab script and Simulink Model, please visit this link: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/hw4/
#  - - -
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @copyright Copyright © 2021 Neil Patel
#  @date May 9, 2021
#
#  @page hw0x05 HW0x05: Full State Feedback
#  @section sec_hw0x05_overview Overview
#  This assignment extends the model developed for HW0x04 to derive the controller 
#  gain values for the ball-plate system. There were four new sections added:
#  - Deriving the K values symbolically from a 4th order generic characteristic 
#  polynomial for a closed loop system.
#  - Finding the pole locations using 2 arbitrarily chosen pole locations (negative), 
#  an arbitrary natural frequency of the system, and an arbitrary damping ratio, 
#  for which another 4th order generic characteristic polynomial is created.
#  - Calculating the K values by equating the polynomials from the prior 2 steps, 
#  and solving for the gain values. Additionally, the calculated gain values are 
#  double checked using Matlab's Control System toolbox.
#  - Finally, the system response with the new gain values is plotted.
#
#  @section sec_hw0x05_res Simulation Results and Performance
#  @image html res1.png width=50% height=50%
#  @image html res2.png "The closed loop results from HW0x04, for comparison with the tuned system response from HW0x05" width=50% height=50%
#  @image html res3.png width=50% height=50%
#  @image html res4.png "The tuned closed loop response for the following gain values: K = [-5.7941 -1.5335 -1.5612 -0.1296]" width=50% height=50%
#
#  Note the significantly lower timescale of the response from the calculated k 
#  values (~0.6s to steady state) when compared to the response from the given 
#  k values (~15s to steady state)- this is much more ideal for the platform 
#  because the ball will be balanced in about 3 "tilts" rather than almost 20 
#  "tilts." This will reduce the load on the motors when balancing the ball, and 
#  will allow the system to respond dynamically to various new stimuli, like a 
#  hand moving the ball after balancing, or the platform getting hit, causing 
#  the ball to move. Additionally, the maximum angle of tilt for the calcualted 
#  k values (~11 degrees), while greater than the maximum angle of tilt for the 
#  given k values by about 3 times, is still reasonably small such that the linear 
#  state space model remains accurate. Additionally, the angular velocity of the 
#  platform, while much greater for the calculated k values, is still low enough 
#  (in magnitude) at maxima and minima that the platform should be able to handle 
#  these speeds. 
#
#  @section hw0x05_source Source Code 
#  For more information on the implementation of this process as well as the 
#  simulink model and foundational Matlab code used in this assignment, please 
#  visit this link: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/hw5/
#  - - -
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @copyright Copyright © 2021 Neil Patel
#  @date May 16, 2021
#
#  @page lab0x01 Lab0x01: FSM Practice and User Interface Development
#  @section sec_lab0x01_summary Summary
#  The objective of this assignment is to develop a a program that embodies the
#  function of the vending machine of assignment HW0x00, where we created a
#  state transition diagram. State transition diagrams are high–level design 
#  tools that can be used to ensure that code will function according to stated
#  design requirements. The Vendotron runs cooperatively; the code does not
#  contain any blocking commands.
#
#  @section sec_lab0x01_features Design Features
#  The Vendotron operates without utilizing any 'blocking' commands and hence,
#  functions cooperatively. The figure below depicts the machine that the
#  Finite State Machine has been modeled after.
#
#  @image html Vendotron.png "Figure 1: The Vendotron Machine (Source: Lab0x01 Handout)"
#  
#  The Vendotron has several buttons including one for each kind of drink that
#  is available. Additionally there is a small two-line LCD text display and
#  coin return button as you would expect on an average vending machine.\n
#
#  The Vendotron exhibits the following features:
#  - On startup, Vendotron displays an initialization message on the LCD 
#  screen.
#  - At any time a coin may be inserted, which results in the balance being
#  displayed on the screen.
#  - At any time, a drink may be selected. If the current balance is 
#  sufficient, the Vendotron vends the desired beverage through the flap at the 
#  bottom of the machine and then computes the correct resulting balance. If 
#  the current balance is insufficient to make the purchase then the machine 
#  displays an "Insufficient Funds" message and then displays the price for 
#  the selected item.
#  - At any time the Eject button may be pressed, in which case the machine 
#  returns the full balance through the coin return.
#  - The Vendotron keeps prompting the user to select another beverage if there 
#  is remaining balance.
#  - If the Vendotron is idle for a certain amount of time, it shows a 
#  "Try Cuke today!" scrolling message on the LCD display.\n
#
#  @section sec_lab0x01_inputs User Inputs
#  <b>Beverage Selection:</b>\n
#  'c' - Cuke\n
#  'p' - Popsi\n
#  's' - Spryte\n
#  'd' - Dr. Pupper\n\n
#
#  <b>Insert Payment</b>\n
#  '0' - Penny\n
#  '1' - Nickle\n
#  '2' - Dime\n
#  '3' - Quarter\n
#  '4' - $1\n
#  '5' - $5\n
#  '6' - $10\n
#  '7' - $20\n\n
#  
#  <b>Return Change</b>\n
#  'e' - Eject change\n\n
#
#  
#  @section sec_lab0x01_diagram Vendotron State Transition Diagram
#  @image html Vendotron_FSM.png "Figure 2: Vendotron State Transition Diagram"
#
#  @section lab0x01_documentation Documentation
#  Documentation of this lab can be found here: Lab0x01
#  @section lab0x01_source Source Code 
#  The source code for this lab: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab1/vendo.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date April 22, 2021
#
#  @page lab0x02 Lab0x02: Interrupt Service Routines and Timing
#  @section sec_lab0x02_summary Summary
#  The objective of this lab is to write some embedded code to develop a
#  program that responds almost instantaneously to an event using MicroPython.
#  We achieved this objective by designing a program that tests the reaction
#  time of a user in response to a light by pressing a button on the Nucleo. We
#  implemented interrupts to achieve this functionality. This assignment was
#  conducted in two parts to navigate the differences in the approaches and to
#  see which approach is more efficient.
#
#  @section sec_lab0x02_features_a Design Features - Part A
#  In the first approach, we implemented the program using the "utime" library.
#  The program waited for a random time between 2 and 3 seconds and the turned
#  on the green LED on the Nucleo. We recorded the time as soon as the LED
#  turned on. We set up an external interrupt on the blue "User" button so that
#  when the button is pressed we can again record the time and compute the
#  difference between the two times to get the reaction time and output it to
#  the terminal. The LED would remain On for only one second. When the program 
#  is stopped, the average reaction time is
#  calculated and displayed on the screen. If no attempts were made, an error
#  message is printed.
#
#  @section sec_lab0x02_features_b Design Features - Part B
#  In the second approach, instead of using the "utime" library, we used built-
#  in features like the timer modules of the STM32 MCU. We configured Timer 2
#  as a free-running counter and designed two callback functions to record
#  the times on two different channels:
#  
#     - The first callback function got triggered by an Output Compare (OC) 
#        compare match on PA5 using Channel 1 of Timer 2. This interrupt was
#        configured so that PA5 went high when the interrupt occurs. We used this
#        to schedule the timing of the green LED. This interrupt was also
#        responsible for turning the LED OFF after one second.
#
#     - The second callback function got triggered by by Input Capture (IC) on 
#        PB3 using Channel 2 of Timer 2. This interrupt was configured so that
#        the timer value was automatically stored in the timer channels capture
#        value whenever the interrupt was triggered. We used this time to compute
#        the reaction time of the user.
#
#  The overall flow of this approach was similar to the first one. It has the
#  same functionality when the keyboard interrupt is pressed as in the first
#  approach and the differnce in precision is neglible to the naked eye.
#
#  @section sec_lab0x02_overview Overview
#  Both approaches achieve the objective of determining the reaction time of
#  the user by incorporating features of the Nucleo. It is observed, however,
#  that utilizing the built-in hardware achieves greater precision while
#  handling the timing. Since this assignment's code is not too heavy on the
#  CPU, the difference in precision is neglible to our objective.
#
#
#  @section lab0x02_documentation Documentation
#  Documentation of this lab can be found here: Lab0x02
#  @section lab0x02_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2a.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2b.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 2, 2021
#
#  @page lab0x03 Lab0x03: Pushing the Right Buttons
#  @section sec_lab0x03_summary Summary
#  The objective of this lab is to build a simple user interface to facilitate
#  communication between a user and laptop. Further, a digital device will be
#  used to measure analog inputs to examine and visualize how input voltage
#  changes as a function of time. We extended our use of interrupts to examine
#  the voltage response of the Nucleo™ to a button press.
#
#  @section sec_lab0x03_features_a Design Features - The Button Circuit
#  The User button on the Nucleo is connected to PC13, which does not have any
#  ADC input functionality. To combat this issue, we connect a jumper from PC13
#  to another pin that has ADC functionality (PA0 in this case). The change
#  made to the circuit is illustrated below:
#
#  @image html buttonCircuit.png "Figure 1: Initial Button Circuit" 
#
#  @image html moddedCircuit.png "Figure 2: Modified Button Circuit"
#
#  @section sec_lab0x03_features_b Design Features - Analog-to-Digital Conversion
#  We used an Analog-to-digital converter (ADC) to translate the continuous
#  analog voltage signal into discrete integers using a 3.3 V reference voltage.
#  We used the maximum 12-bit resolution to examine the board's voltage response.
#  Since the User button pin did not have any ADC functionality (as described in the
#  previous section), we had to jumper that pin to another pin designed
#  excclusively for Analog operations. After doing this, we were successfully
#  able to record the ADC readings of the voltage response and store it in a
#  buffer for further use.
#
#  @section sec_lab0x03_features_c Design Features - Serial Communication
#  To wrap everything together, we implemented Serial Communication using UART
#  to practically create a "virtual handshake" between the front end and the
#  Nucleo™.
#
#  @image html frontEnd.png "Figure 3: Serial Communication"
#
#  To set up the serial communciation properly, we first created a script that
#  would establish communiations with the Nucleo™. Then we created a script that
#  facilitated the software handshake between the board and the User Interface.
#  After that connection was succesfully made, we focused on capturing the
#  correct data from the ADC and successfully transmitting it over to the front
#  end. Once we successfully transferred the data, we used it to create the best fit
#  plot and a CSV file containing the data.
#
#  @section sec_lab0x03_overview Overview
#  We were successfully able to derive a plot of values that matched the
#  theoretical description. We accomplished this by finding the buffer in
#  which the greatest change took place by comparing the first and last value.
#  The plot obtained by doing so is displayed below.
#
#  @image html stepResponsePlot.png "Figure 4: ADC counts vs Time Plot"
#
#  Theoretically, the time constant RC = 0.5 ms. To get the time constant from
#  the plot, we found out the corresponding x value to y = 63.2% of VDD = 2588.04
#  ADC counts. The time constant using this method came out to be around 0.6 ms,
#  which is fairly close to our theoretical value. The equation and trendline
#  used to calculate this value is shown in the figure below. Notice that the
#  graph below does not consist data points when the ADC count is 0 to make the
#  process of determining time constant easier. Also notice, that the graph
#  has an x-intercept ofaround 1 ms, so that is also taken into consideration
#  when calculating the time constant.
#
#  @image html plotTrendline.png "Figure 5: Trendline Equation Plot"
#
#  @section lab0x03_documentation Documentation
#  Documentation of this lab can be found here: Lab0x03 \n
#  A copy of the CSV file of the good step response test can be found here: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/voltvstime.csv
#  
#  @section lab0x03_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/UI_front.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/main.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 6, 2021
#
#  @page lab0x04 Lab0x04: Hot or Not?
#  @section sec_lab0x04_summary Summary
#  The objective of this lab is to create and test a driver for an I2C-connected
#  sensor and use that sensor to log some data. The sensor in question is going
#  to be an MCP9808 temperature sensor and the data collected will be the
#  surrounding ambient temperature in an 8 hour window.
#
#  @section sec_lab0x04_features Design Features
#  The MCP9808 temperature sensor uses an I2C interface to communicate with the
#  MCU. In this assignment, the MCP can be utilized using only its power, ground,
#  and two I2C communication pins (SDA and SCL). A pin diagram for the MCU-MCP 
#  connection is shown below:
#  
#  @image html pinDiagram.png "Figure 1: Pin Diagram for I2C Connection"
#
#  This program contains a class that allows the user to communicate with the
#  MCP using its I2C interface. The class is able to initialize itself using
#  an I2C object and the MCP address on the I2C interface, verify that the sensor
#  is attached at the given bus address, and measure the temperature in degrees
#  Celcius and Fahrenheit. The program also measures the internal temperature
#  of the MCU and records both temperatures in degrees Celcius every 60 seconds.
#  Data is taken until the user presses "Ctrl+C" or till the time limit is
#  reached. Data is then saved in a CSV file and plotted.
#
#  @section sec_lab0x04_overview Overview
#  Since the sensor was placed in a room which was thermally regulated by a
#  thermostat, the temperature vs. time plot is nearly a straight line. Same
#  case with the MCU temperature readings as the temperature measured is internal
#  to the microcontroller. The y-axis is temperature in degrees Celcius and the
#  x-axis is time in seconds. The readings were taken in an 8-hour window from
#  2 pm to 10 pm. Series 1 is the internal temperature of the MCU (steady at 
#  rougly -5 degrees Celcius) and Series 2 is the readings from the temperature
#  sensor (steady at roughly 22 degrees Celcius). The plot is shown in the 
#  figure below:
#
#  @image html tempVsTime.png "Figure 2: Temperature vs. Time Plot"
#
#  @section lab0x04_documentation Documentation
#  Documentation of this lab can be found here: Lab0x04 \n
#  A copy of the CSV file can be found here:
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/temps.csv
#  
#  @section lab0x04_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/mcp9808.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/main.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 13, 2021
#
#  @page lab0xff Lab0xFF: Term Project
#  @section sec_lab0xff_summary Summary
#  The objective of this lab is to create a culminating term project that utilizes
#  mechatronics and control skills by balancing a ball on top of our mechatronics
#  kit. This project builds upon the system modelling and calculations done in
#  HW0x02, HW0x04, and HW0x05. The project was broken into segments that dealt
#  with assembling separate parts and task, and then integrating them to achieve
#  functionality.
#
#  @image html fullSystem.png "Figure 1: Mechatronics Kit" width=50% height=50%
#
#  @section sec_lab0xff_features Design Features
#  The individual parts that made up the system were the Touch Panel, Motors,
#  and Encoders. The model of the platform was used to build the Controller, 
#  which relies on the ball's position data (X,Y) from the touch panel and the 
#  motor angle data (theta_X, theta_Y) from the encoder, as well as their derivatives 
#  velocity and angular velocity. The final step to the project was to run all 
#  4 segments together. This required a Scheduler to make sure all tasks ran 
#  cooperatively. Much of this was done through cotask.py which was provided by 
#  the instructor for this class. To easily share data (such as position and 
#  angle measurements), a second provided module called task_share.py was used.
#
#  @section sec_lab0xff_touch_panel Touch Panel
#  The Touch Panel Sensor's purpose is to determine the x and y position of the 
#  ball on the platform. \n
#  By controlling four pins (Two in the "X" direction and two in the "Y" direction), 
#  the distance in mm from the origin (the center of the panel) can be found. 
#  There are 3 primary functions to the Touch Panel Driver:
#     - scan_X(): Returns the X coordinate in mm
#     - scan_Y(): Returns the Y coordinate in mm
#     - scan_Z(): Returns True if ball (or another object) is in contact with the 
#       panel, else returns False
#  .
#  The scan_ALL() function can be used to conveniently perform all 3 measurements. 
#  More documentation can be found at touchPanel.py. \n
#  The Resistive Touch Panel works by having two wires that wind back and forth 
#  from on side to the other side of the panel. One wire's resistance only increases 
#  by a noticeable amount in the X direction, while the other only in the Y direction. 
#  If you were to look at a point 2/3 of the way through the panel, then 2/3 of 
#  the resistance of the wire is before that point, and 1/3 after. When the 
#  touch panel is touched, the wires short together at that point. This allows 
#  the position of the touch to be measured. Each direction is measured individually 
#  by setting one side of the wire for that direction to 3.3 V, and the other 
#  to ground. The wire that is not being measured is connected to an ADC on one 
#  side while the other side is allowed to float. This allows the voltage at the 
#  intersection to be measured, which can be used to determine the position in 
#  that direction thanks to the linear increase of the wires resistance. To 
#  determine if something is touching the panel at all, one wire has one end 
#  connected to 3.3 V, while the other has one end connected to 0 V. If the 
#  other end of the 0 V connected wire is measured to also be 0 V, then the 
#  wires cannot be connected together, so the panel is not being touched. If the 
#  value read is nonzero, then the wires must have been connected and the panel 
#  was touched. \n
#  A simplified circuit diagram for the touch panel is shown below. The pins 
#  (Yp, Ym, Xp, Xm) in this case are configured for scan_Z - determining if the 
#  panel is being touched.
#
#  @image html tpCircuitDiagram.png "Figure 2: Circuit Diagram for the Touch Panel" width=50% height=50%
#
#  When the touch panel is not being touched, the two wires are not connected to 
#  each other, and in this case Xp is tied to 0 V while Ym is tied to 3.3 V. When 
#  the panel is touched at point P, the two wires intersect. Since Xp is no longer 
#  0 V and Ym is no longer 3.3 V, we can determine that the touch panel is detecting 
#  something. To find the position itself, we just find the Voltage at point P - 
#  the position in each direction will be the Voltage at point P as a percent of 
#  the input voltage multiplied by the total length of the panel in that direction. \n
#
#  @image html tpPinOut.png "Figure 3: Pin Layout for the Touch Panel" width=50% height=50%
#
#  Below is an image of the touch panel on the platform, with nothing touching it. 
#  This was used to test the scan_Z function in the benchmarks.
#
#  @image html tpTop.png "Figure 4: Touch Panel Top View" width=50% height=50%
#
#  Below is an image of the touch panel on the platform, with the ball that it 
#  senses on top. This is the position used to measure the benchmark data.
#
#  @image html tpTopBall.png "Figure 5: Touch Panel with Ball" width=50% height=50%
#
#  Below is an image of the output of the functioning TouchPanel in PuTTY, 
#  including the scan_ALL and the total time for each operation.
#
#  @image html tpTest.png "Figure 6: Touch Panel Output" width=50% height=50%
#
#  @subsection subsec_tp_calib Calibration of Touch Panel
#  The Panel dimensions were found to be 192mm x 112mm. However, measurements 
#  did not always function on the border, so the possible measurement ranges are below:
#      - minimum X: -84 mm
#      - maximum X: +84 mm
#      - minimum Y: -48 mm
#      - maximum Y: +42 mm
#  .
#  The minimum and maximum Y values likely differ since the panel is slightly 
#  off center in the Y direction - there is an extra couple millimeters of dead 
#  space on the negative y side to give room for the wires used to connect it 
#  to the MCU. As a result, the measured center is slightly above the center of 
#  the panel, by about 3 mm. Otherwise, the center measurement was found to be 
#  spot on - within a millimeter at least, though I lack the measurement tools 
#  to determine its exact position. However, measurements further out from the 
#  center became much less accurate, since the resistance of the panel did not 
#  seem to be linear.
#
#  @subsection subsec_tp_bench Benchmarks for Timing
#  Each function was tested by running it 50 times in the benchmark. Timing was 
#  done using the ticks_us function from the utime library.
#
#  @subsubsection sss_scanz Scan_Z Test:
#  With the ball off of the platform:
#      - All 50 results were False, which is correct.
#      - The Total benchmark time was 18392μs
#      - The Average function time was <b>367.84μs</b>
#
#  With the ball on the platform:
#      - All 50 results were True, which is correct.
#      - The Total benchmark time was 18403μs
#      - The Average function time was <b>368.06μs</b>
#
#  @subsubsection sss_scany Scan_Y Test:
#  With the ball on the platform at around Y = -28mm: 
#   - The maximum variation in Y was 0.073 mm
#   - The Standard Deviation in Y was 0.02 mm
#   - The Total benchmark time was 19712μs
#   - The Average function time was <b>394.2μs</b>
#
#  @subsubsection sss_scanx Scan_X Test:
#  With the ball on the platform at around X = 20.7mm: 
#   - The maximum variation in X was 0.17mm
#   - The Standard Deviation in X was 0.038 mm
#   - The Total benchmark time was 19568μs
#   - The Average function time was <b>391.36μs</b>
#
#  @subsubsection sss_scan_all Scan_ALL Test:
#  Testing all three functions together in the scan_ALL() function:
#   - The Total benchmark time was 55139μs
#   - The Average function time was <b>1102.78μs</b>
#
#  @subsubsection sss_summary Summary of Benchmarks
#  All three basic scan functions meet the required specs: they all measure under 
#  500 microseconds individually. As a result the scan_ALL() function can be performed 
#  in about 1.1 ms, which is easily faster than the estimated 1.5ms needed for 
#  the system to function smoothly. Both X and Y measurements were accurate across 
#  many trials, with only small variations. The X measurements varied more than 
#  the Y measurements most likely because the X axis is longer. The scan_Z() function 
#  was equally consistent in identifying if the ball was in contact with the touch 
#  panel. 
#
#  @subsection subsec_tp_usage Usage of Touch Panel Driver
#  To use the touch panel with the driver, initialize an object for the class 
#  and assign the correct pins for each input. For example, if yp from the panel 
#  is connected to the PA6 pin on the MCU, then use yp=pyb.Pin.board.PA6 when 
#  initializing. Then, scan_Z() can be used to check if something is touching 
#  the board before finding the position by calling scan_X() and scan_Y(), and 
#  then scan_ALL() can be used to conveniently check all three - it will return 
#  a tuple with (Z, X, Y).
#
#  @section sec_lab0xff_motors Motors
#  The Motor Driver allows the DC motors to be driven at different power levels 
#  using PWM. \n
#  The user can specify the Duty cycle for each motor. If a fault occurs 
#  (such as when something is blocking the motor) the motor will automatically 
#  be disabled. It can be re-enabled with clear_fault(). The motors can also be 
#  enabled or disabled with enable() and disable() respectively. Upon initializing 
#  the driver, takes the pins of the motor as well as the fault and sleep pins. 
#  Documentation can be found at motor.py. \n
#  Below is an image of the two motors. 
#  To set dutycycle, use set_level_1() for Motor 1 and set_level_2() for motor 2. 
#  Duty cycle is used as a percent not a decimal. 
#
#  @image html mots.png "Figure 7: Motors on the Mechatronics Kit" width=50% height=50%
#
#  Each motor has 2 inputs to control direction and torque. The two motors share 
#  the input nSLEEP, which tells the motors when not to sleep. They also both 
#  share the fault pin, nFAULT, which goes low whenever a fault occurs in one 
#  of the mots and must be cleared before operation may continue. The affect of 
#  the inputs on motor operation is shown below.
#
#  @image html motTT.png "Figure 8: Motor Input Truth Table" width=50% height=50%
#
#  The motors spin when one input is high and the other low. With constant input 
#  voltages, the motors behave in an on or off manner In order to adjust the 
#  power of the motor, Pulse Width Modulation (PWM) was used. This allowed the 
#  dutycycle of the input wave to be adjusted. At 100% duty cycle, when the wave 
#  is just a dc constant, the motors operate at full power. By decreasing the 
#  percent of time the wave is high compared to low, the power delivered to the 
#  motors can be decreased. The motor pins and their corresponding pins on the 
#  board are shown below. IN1/IN2 correspond to Motor 1, while IN3/IN4 correspond 
#  to Motor 2. The inputs are connected to a Timer for PWM. 
#
#  @image html motPinOut.png "Figure 9: Pin Layout for the Motors" width=50% height=50%
#
#  @subsection subsec_mot_video Video Demonstration
#  Below is a demonstration of motor 1 in action. It is being started at 0% duty 
#  cycle and the duty cycle is increased by 1% every half second, until it reaches 
#  a duty cycle of 100%. At that point a fault is triggered by forcefully grabbing 
#  onto the motor, and the motor stops. (Note: the motor is grabbed before it 
#  reaches 100%, so it does not trigger a fault at first).
#
#  \htmlonly
#  <iframe width="560" height="315" src="https://www.youtube.com/embed/dSEngHIxrGE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#  \endhtmlonly
#
#  @subsection subsec_mot_benchmark Dutycycle Benchmark
#  To test the minimum duty cycle needed for the motors to overcome static friction 
#  and begin spinning, each one was set to 0% duty cycle, and then duty cycle 
#  was increased in intervals of 1% until the motor began moving consistently. \n
#  For Motor 1:
#       - At ~20% dutycycle, motor begins inconsistent, stop and go movement.
#       - Direction Counterclockwise: 27% dutycycle needed
#       - Direction Clockwise: 28% dutycycle needed
#
#  For Motor 2:
#       - At ~28% dutycycle, motor begins inconsistent, stop and go movement.
#       - Direction Counterclockwise: 33% dutycycle needed
#       - Direction Clockwise: 34% dutycycle needed
#
#  @section sec_lab0xff_encoders Encoders
#  The Encoder Driver is used with the encoders to update the angle / angular 
#  velocity of the motors. \n
#  Like with the motors it initializes with the pins that 
#  connect to the encoders. The Encoder Driver class includes a method for updating 
#  the current position of the motor with update(), and a separate method for 
#  retrieving the current position (get_position()). There is also a method for 
#  the change in angle from the previous update (the angular velocity) and one 
#  to manually reset the current angle to a chosen value. Documentation can be 
#  found at encoder.py. \n
#  The encoder driver automatically handles overflow (or underflow) by noting 
#  that if the tick count for the encoder has changed by over half its possible 
#  values, then it most likely has reached overflow, and then compensating by 
#  adding or subtracting 65535 (the total number of ticks in the encoder 
#  measurement). \n
#  To test that the encoders were working, the platform was tilted back and forth. 
#  The tick counter adjusted with the platform and increased in one direction 
#  but decreased in the other. From the starting position, it was possible to 
#  reach a negative position which indicated that the underflow was working correctly. 
#  Since overflow works similarly but in the opposite direction, it was assumed 
#  that overflow would also work fine. The video below demonstrates the usage 
#  of one of the encoders while tilting the platform. The data can be seen listed 
#  in the terminal - the units are ticks, and there are around 4000 ticks to a 
#  rotation on the encoder. Apologies for the vertical format.
#
#  \htmlonly
#  <iframe width="560" height="315" src="https://www.youtube.com/embed/edoz526r8vA" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#  \endhtmlonly
#
#  The Encoder pins are attached as described in the table. The pins they are 
#  connected to are connected totimers which support encoder functionality. These 
#  are necessary since the timing for encoders is too sensitive to do using pure 
#  software.
#
#  @image html encPinOut.png "Figure 10: Pin Layout for the Encoders" width=50% height=50%
#
#  @section sec_lab0xff_controller Controller
#  The Controller object uses the input data of position, theta, velocity, and 
#  angular velocity to determine the correct duty cycle for the motor according 
#  to the system model. \n
#  When instantiating, it takes the K constants from HW0x05, [k1 k2 k3 k4] as 
#  arguments. The controller needs to be defined in both the x and y direction - 
#  each controller takes one set of inputs and controls one motor. The only 
#  method in this class is DutyCycle(x, theta, x_dot, theta_dot), which returns 
#  the correct duty cycle as a percent for the motors. More on usage can be 
#  found at controller.py. \n
#  The Controller calculates Torque with T = -x*k, or T = (k1*x + k2*theta + 
#  k3*velocity + k4*ang_velocity). Using the Torque, it finds the duty cycle 
#  using the Torque Constant K of the motors (13.8 mNm/A) and the DC resistance 
#  (2.21 Ohms). The Duty cycle was found using the equation:
#
#  @image html dutyEq.png "Figure 11: D = 100% * T * (R / (N*V*K))" width=50% height=50%
#
#  @image html consts.png "Figure 12: Constants R, N, K, and V" width=50% height=50%
#
#  Where K' is the vector of K values described above and found in HW0x05, and 
#  the X vector contains the input parameters position, angle, velocity, and 
#  angular velocity. The constants in from HW0x05 were adjusted quite a bit to 
#  fit the system in the real world. This is likely due to a variety of factors 
#  including: unideal motors, inaccuracy in angle and position measurement, 
#  small timing errors leading to small differences in velocity measurements, 
#  friction, and other factors not taken into account in the theoretical 
#  system model. 
#
#  @section sec_lab0xff_scheduler Scheduler
#  The Scheduler, main.py, defines tasks for each of the above segments (TouchPanel, 
#  Motors, Encoders, and the Controller). \n
#  The Encoder and Touch Panel tasks are fairly straight forward - they update 
#  position and angle respectively, and then save it to a shared queue so that 
#  the other tasks may access it. The shared queue is provided by task_share.py. 
#  The Controller Task creates two instances of the Controller class using specified 
#  K values, one for each direction, and then uses the data from the Touch Panel 
#  and Encoders to get a duty cycle, which it also adds to a shared queue. The 
#  Motor Task then runs and sets the motors to the correct duty cycle. The tasks 
#  are set so that the Encoder and Touch Panel tasks run once every 40 ms, and 
#  the other two tasks run once every 15 ms. A list of scheduler tasks and shared 
#  queues and their descriptions is shown below.
#
#  @image html taskShare.png "Figure 13: Tasks and Queues"
#
#  @section sec_lab0xff_std State Transition Diagram
#  The state transition diagram for the project is shown below.
#
#  @image html std.png "Figure 14: State Transition Diagram" width=50% height=50%
#
#  @section sec_lab0xff_vd Video Demonstration
#  Neil's working platform is demonstrated below. The video contains a brief 
#  explanation of our procedure:
#
#  \htmlonly
#  <iframe width="560" height="315" src="https://www.youtube.com/embed/ii6B9aYJZHk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#  \endhtmlonly
#
#  Neil's platform is a fair bit better at balancing the ball, and keeps steady 
#  while the ball is placed. It then moves the ball to a spot close to the center, 
#  though it does not get to the exact center. \n
#  Craig could not demo the project on his own hardware, as he accidentally broke 
#  his board (see Board Issues). This is the last recording of his still working 
#  platform:
#
#  \htmlonly
#  <iframe width="560" height="315" src="https://www.youtube.com/embed/FQxBvJwhmho" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
#  \endhtmlonly
#
#  While Craig did not have enough time to fully finish tuning his program, the 
#  platform clearly reacts to the touch and slowly moves back to equilibrium, 
#  though it somewhat overreacts due to its not optimal constant values for the 
#  controller.
#
#  @section sec_lab0xff_pd Platform Data
#  Sample data taken during this process can be found in Documentation. Each .csv 
#  file has counts in the first column. To get to time, multiply by the period of 
#  Controller, or 15ms per entry. The second column includes the actual measurements. 
#  Data was not all from the same run. Data naturally varies from test to test, 
#  depending on ball placement, so this data does not reflect what would occur 
#  every time. The data of x and y positions are plotted below:
#
#  @image html x_data.png "Figure 15: X Position of the Ball Over Time" width=50% height=50%
#
#  The X position of the ball started a little over halfway to the edge of the 
#  touch panel, at -40 mm. From there it is quickly balance to close to 0 mm, 
#  before over correction causes it to roll a bit further away. At time 1275 ms, 
#  the touch panel seems to have a small error and defaults values to 0. Luckily 
#  this is quickly corrected and the ball continues moving in the right direction.
#
#  @image html y_data.png "Figure 16: Y Position of the Ball Over Time" width=50% height=50%
#
#  Similar to the X data, the Touch panel gives a couple incorrect data measurements 
#  for the Y position around 1550 ms in. The 50mm measurement was probably caused 
#  by detecting the ball, but having the ball lose contact with the panel before 
#  sensing its location. As a result, it detected the ball in the furthest position. 
#  For the most part the ball started fairly close to the x axis here, so little 
#  correction was needed.
#
#  @section sec_lab0xff_mi Major Issues
#  A fair few problems cropped up, especially when putting everything together:
#
#  @subsection subsec_tp_issues Touch Panel Issues
#  The touch panel had a few problems. The most concerning was a flaky connection 
#  along one of the wires connecting it to the microcontroller board. This resulted 
#  in the panel occasionally losing connection and giving completely false data 
#  until the wire was readjusted. This error came and went, so was left unresolved 
#  since the panel still worked fine <b>most</b> of the time. \n
#  Another major difficulty was the inaccuracy of the touch panel. The readings 
#  at either border were not always consistent - while measurements were fairly 
#  accurate nearer to the center of the panel (after calibration at least), the 
#  non-linear resistance of the device made measurements less accurate the further 
#  out you went. The border was also not exactly at the edge of the panel - 
#  touching the panel right on the corner gave nonsensical data. This problem 
#  gave problems for the controller, since it relied on accurate data to decide 
#  on a torque for the dc motors. Putting the ball too far from the platform made 
#  the measurements too inaccurate to work. \n
#  One last issue of note, which is fairly amusing looking back, was when a very 
#  small sticker somehow attached itself to the side of the panel. This resulted 
#  in the panel identifying a touch on that edge, and many, many hours were spent 
#  trying to determine the cause. This issue disappeared when the sticker was 
#  noticed and removed.
#
#  @subsection subsec_mot_issues Motor Issues
#  The Motors worked fairly well, and PWM was relatively easy to implement. The 
#  biggest problem stemmed from the two motors having different torques at the 
#  same power. One motor was significantly weaker than the other. This was likely 
#  just due to a manufacturing difference, since they are the same model of motor. 
#  However, it created problems for the controller, as it needed to be adjusted 
#  so that one motor wasn't running much harder than the other. \n
#  Another problem encountered with the motors was that the shaft was too loose
#  for the platform arms, which led the motor to spin in place without moving the
#  arms at all. There were three fixes for this problem - tightening the screw
#  on the platform arm,, super-gluing the the shaft to the arm, and somehow filling
#  the space between the shaft and the arm. Tightening the arm was out of question
#  since the heads of the screws were messed up, probably due to previous usage.
#  The other two options were pretty terminal (if you chose them, it would take
#  some time to undo and redo in case of any backtracking). Ultimately, Neil
#  decided to put some tape on the motor shaft so that the arm-shaft connection
#  is tighter and actually moves the platform.
#
#  @subsection subsec_enc_issues Encoder Issues
#  The encoders worked quite well from the start, however, the original implementation 
#  only handled overflow and not underflow. This created some odd data before 
#  the issue was resolved. There was also a question of an issue with accuracy 
#  as the belt tying the motor and the encoder would sometimes slip, but this 
#  issue seemed negligible.
#
#  @subsection subsec_sched_issues Scheduler Issues
#  The scheduler was easily the hardest part to get working, and as this is written, 
#  is still not fully operationsal - it still lacks a proper UI. It does manage 
#  all the tasks well and they all run quickly and at the appropriate time. There 
#  were some issues with using too small of a period for the tasks, which caused 
#  them to be unable to complete on time. This was resolved by increasing the 
#  time each task would run for. The biggest problem with the scheduler was trying 
#  to get the correct K constant values. No matter how they were adjusted, they 
#  didn't seem to work.
#
#  @subsection subsec_board_issues Board Issues
#  On Craig's board, a screw came loose and fell onto the MCU board. This caused 
#  a short between two pins - it even gave off a slight spark - and the board 
#  stopped working. Even replacing the Nucleo board with a new one did not resolve 
#  the issue, and the voltage at various pins was incorrect - so some damage must 
#  have been done to other components as well. This prevented Craig from getting 
#  the project running on his own hardware.
#
#  @section lab0xff_documentation Documentation
#  Documentation of this lab can be found here: Lab0xFF \n
#  A copies of the CSV files can be found here:
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/collectedData.csv \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/xData.csv \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/xDotData.csv \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/yData.csv \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/yDotData.csv
#  
#  @section lab0xff_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/controller.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/encoder.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/main.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/motor.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/labff/touchPanel.py
#
#  @section lab0xff_repos Repositories
#  The repositories of the students involved in this project can be found at: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/ \n
#  https://bitbucket.org/cbelshe/me405_craigbelshe/src/master/
#  - - -
#  @author Group 2: Neil Patel, Craig Belshe
#  @copyright Copyright © 2021 Neil Patel
#  @date June 11, 2021