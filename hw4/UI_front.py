""" @file UI_front.py
    @brief This file contains the UI front-end which runs on the PC and
           receives user input.
    @package Lab0x03
    @brief This package contains UI_front.py, main.py
    @author Neil Patel
    @date May 6, 2021
"""
import serial
import keyboard
import array
from matplotlib import pyplot

## @brief Implementation of serial communication
ser = serial.Serial(port = 'COM5', baudrate = 115200, timeout = 5)
## @brief The input key pressed by the user.
last_key = ''
## @brief Array keeping track of time stamps
time = array.array('f')
## @brief Array keeping track of ADC counts
data = array.array('f')

def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
        @param key String representation of the key pressed by the user.
        @return last_key String representation of the last key pressed by the 
                user, stored as a global variable.
    """
    global last_key
    last_key = key.name
    
# Tell the keyboard module to respond to this particular key only
keyboard.on_release_key("g", callback=kb_cb)

def sendVals():
    """ @brief Callback function which is called when a key has been pressed.
        @details Waits for the 'G' key press from the user. Once the key is
                 pressed, sends the character over to the Nucleo to signal the
                 start of data capture. Receives the captured data back from
                 the Nucleo™ and arranges it into the time and data arrays.
        @return Returns nothing.
    """
    ## @brief Keeps track of the last key pressed by the user (global variable).
    global last_key
    print('Press G to start the process.')
    while True:
        if last_key == 'g':
            print('The voltage readings of the User button press in the next',
                  '5 seconds will be documented.')
            break
    # Sends the encoded character over the serial.
    ser.write('g'.encode())
    while True:
        # Decodes the received captured data and compartmentalizes it.
        line = ser.readline().decode()
        if not line or line == '\x04\x04>':
            break
        split_line = line.split()
        time.append(float(split_line[0])/1000.0)
        data.append(float(split_line[1]))

for n in range(1):
    sendVals()

# Termination of serial and keyboard.    
keyboard.unhook_all()
ser.close()

# Plots the captured values of the timestamps and ADC counts    
pyplot.figure()
pyplot.plot(time,data)    
pyplot.xlabel('Time, t, in ms')
pyplot.ylim([0, 4095])
pyplot.yticks([0, 1020, 2040, 3060, 4095])
pyplot.ylabel('ADC counts')
pyplot.title('V vs t')

# Writes the captured data into a CSV file
file1 = open('voltvstime.csv', 'w')
for n in range(len(time)):
    line = "%f, %f\n" % (time[n], data[n])
    file1.write(line)
file1.close()
