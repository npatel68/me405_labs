var namespaces_dup =
[
    [ "adc", null, [
      [ "testADC", "adc_8py.html#ae8c6eca91d42c1b3323dd30cc9e8fc14", null ],
      [ "adc", "adc_8py.html#a26f07d0d11df2a0cf58cbae8ae0eac6c", null ],
      [ "buffy", "adc_8py.html#a39ab43aff1a75658228f8853b95328ed", null ],
      [ "pina0", "adc_8py.html#acb41a5fec6764e8e758d1c9719b53b4e", null ],
      [ "pinc13", "adc_8py.html#acc0291ec7cf8f3ae1e9c27f2ad39ca08", null ]
    ] ],
    [ "intTemp", null, [
      [ "adc", "intTemp_8py.html#acd878249293e06e2b6534ba2919b4070", null ],
      [ "i", "intTemp_8py.html#a8286580d2f21ecd01c22d634aab410d4", null ],
      [ "val", "intTemp_8py.html#a3c8c8105cc302e0cf7dce555a19e4778", null ]
    ] ],
    [ "Lab0x01", "namespaceLab0x01.html", null ],
    [ "Lab0x02", "namespaceLab0x02.html", null ],
    [ "Lab0x03", "namespaceLab0x03.html", null ],
    [ "Lab0x04", "namespaceLab0x04.html", null ],
    [ "lab2a", null, [
      [ "record_time", "lab2a_8py.html#a807569abb08ba2ec8a35492f2f57ef25", null ],
      [ "blinko", "lab2a_8py.html#ac35968841de386035fbeb49f88209d6d", null ],
      [ "pinc13", "lab2a_8py.html#abc996688dd1149ec316feeaf52b6028f", null ],
      [ "tries", "lab2a_8py.html#a0e52513e6647d3a8e3d440bb786207f2", null ]
    ] ],
    [ "lab2b", null, [
      [ "cb1", "lab2b_8py.html#a97ad1e4fbbac7a8d6d95ed0fb871d6b4", null ],
      [ "cb2", "lab2b_8py.html#a42a75dd4fb564ee78eda4c87f0ec4a3d", null ],
      [ "ch1", "lab2b_8py.html#a08d48779e326982639a78e5e4aa5420c", null ],
      [ "ch2", "lab2b_8py.html#af507ff31a83e5c09597b0f12d4610b29", null ],
      [ "comp", "lab2b_8py.html#a57c1b7fc84742ac1f446201189ecfd98", null ],
      [ "count", "lab2b_8py.html#adff53c8d4117aff4fbd01d10ff38f8d0", null ],
      [ "pina5", "lab2b_8py.html#a6c5c8e06719139c3f293db696dbe2770", null ],
      [ "pinb3", "lab2b_8py.html#a5cae82f0f114d3aba2d789635e8c0e2f", null ],
      [ "pinc13", "lab2b_8py.html#ab6f4be640dfe20a77e5f0edac3e99b0c", null ],
      [ "rTime", "lab2b_8py.html#a536dc98897ed4c3dccedbda3c0428b2d", null ],
      [ "timmy", "lab2b_8py.html#a6a7da999aefdca5b6c672c39de077d25", null ],
      [ "tries", "lab2b_8py.html#a75efbea126950aab12cec98fde20b2fa", null ]
    ] ],
    [ "main", null, [
      [ "adc", "main_8py.html#afac357daa82cb1f4a2ca28df4f60bb14", null ],
      [ "baudrate", "main_8py.html#ac14aac87c9f29dd54a04665a40956cee", null ],
      [ "fin", "main_8py.html#ad4ac2e31f6844e4206648a294ef4dbab", null ],
      [ "i2c", "main_8py.html#ab2bba10cbf0946121616503df3750664", null ],
      [ "MASTER", "main_8py.html#a4b3139cb52bde00a56f7a4ca68d77339", null ],
      [ "rec_time", "main_8py.html#a558f01093bc46c729a8599271b570bd5", null ],
      [ "sensor", "main_8py.html#a86cc0c2d3b40e8d33d8dd98b459621a2", null ],
      [ "start", "main_8py.html#a081681814154296ba00d84fdf98028f7", null ],
      [ "temp1", "main_8py.html#ac76e821fc54a4844c16a17522b6828a2", null ],
      [ "temp2", "main_8py.html#af2126d60c018a861342b25d49487e3cd", null ]
    ] ],
    [ "mcp9808", null, [
      [ "iic", "classmcp9808_1_1iic.html", "classmcp9808_1_1iic" ],
      [ "baudrate", "mcp9808_8py.html#ac9867d0c323a356515d552b55159e6e5", null ],
      [ "eye2see", "mcp9808_8py.html#ac5462e8415c1d9817996ca80403a1f13", null ],
      [ "i2c", "mcp9808_8py.html#ae99e487519ec535e9356de31b7f1dee7", null ],
      [ "MASTER", "mcp9808_8py.html#a836d2d0f8fafb6bd9bb05908da0b0a29", null ]
    ] ],
    [ "UI_front", null, [
      [ "kb_cb", "UI__front_8py.html#aa21ab56e8f0029f944c615900213ebdd", null ],
      [ "sendVals", "UI__front_8py.html#a91eed7dff1ab057279b1dd00b5b3959d", null ],
      [ "data", "UI__front_8py.html#a6fd3153bbe3baed376cd2dcdad579244", null ],
      [ "file1", "UI__front_8py.html#a1de879849ea08fd082f0c72f45118801", null ],
      [ "last_key", "UI__front_8py.html#ab06f43189082ba23a4383d5d65049704", null ],
      [ "line", "UI__front_8py.html#a11e27c46d3bd2c49d02df2423d3d7bca", null ],
      [ "ser", "UI__front_8py.html#a26d65cb7ea7d47e295007598eedfb6ba", null ],
      [ "time", "UI__front_8py.html#aa88408f92efb28993de95812284a0e34", null ]
    ] ],
    [ "vendo", null, [
      [ "kb_cb", "vendo_8py.html#a92b1e3a4a1d1398889f106f18cf46c6e", null ],
      [ "VendotronTask", "vendo_8py.html#a9fed49d43101743f5a8240f570f8727c", null ],
      [ "last_key", "vendo_8py.html#a10853866c82014f11d5621e87b52616e", null ],
      [ "vendo", "vendo_8py.html#afc7c9223cbbe0c449c296a7b3c5fbb99", null ]
    ] ]
];