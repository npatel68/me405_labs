""" @file adc.py
    @brief Test script for the ADC.
    @author Neil Patel
    @date May 6, 2021
"""
#import keyboard
import pyb
import micropython
import array


buffy = array.array('H', (0 for index in range(200)))        


def testADC():
    global buffy
    print("Press button.")
    adc.read_timed(buffy, 40)
    for val in buffy:
        print(val)
   

if __name__ == '__main__':
    
    pinc13 = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
    pina0 = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
    adc = pyb.ADC(pina0)
    
    try:
        micropython.alloc_emergency_exception_buf(100)
        testADC()
        
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        