""" @file intTemp.py
    @brief This file contains a script for the microcontroller that measures
           the internal temperature of the MCU and saves it to a file.
    @author Neil Patel
    @date May 13, 2021
"""

import pyb

adc = pyb.ADCAll(12, 0x70000)

with open ("temps.txt", "w") as file1:
    i = 0
    while (i < 100):
        val = adc.read_core_temp()
        file1.write("Temperature is: %d degrees C\n" %val)
        i += 1
file1.close()
    