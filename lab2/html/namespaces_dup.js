var namespaces_dup =
[
    [ "Lab0x01", "namespaceLab0x01.html", null ],
    [ "Lab0x02", "namespaceLab0x02.html", null ],
    [ "lab2a", null, [
      [ "record_time", "lab2a_8py.html#a807569abb08ba2ec8a35492f2f57ef25", null ],
      [ "blinko", "lab2a_8py.html#ac35968841de386035fbeb49f88209d6d", null ],
      [ "pinc13", "lab2a_8py.html#abc996688dd1149ec316feeaf52b6028f", null ],
      [ "tries", "lab2a_8py.html#a0e52513e6647d3a8e3d440bb786207f2", null ]
    ] ],
    [ "lab2b", null, [
      [ "cb1", "lab2b_8py.html#a97ad1e4fbbac7a8d6d95ed0fb871d6b4", null ],
      [ "cb2", "lab2b_8py.html#a42a75dd4fb564ee78eda4c87f0ec4a3d", null ],
      [ "ch1", "lab2b_8py.html#a08d48779e326982639a78e5e4aa5420c", null ],
      [ "ch2", "lab2b_8py.html#af507ff31a83e5c09597b0f12d4610b29", null ],
      [ "comp", "lab2b_8py.html#a57c1b7fc84742ac1f446201189ecfd98", null ],
      [ "count", "lab2b_8py.html#adff53c8d4117aff4fbd01d10ff38f8d0", null ],
      [ "pina5", "lab2b_8py.html#a6c5c8e06719139c3f293db696dbe2770", null ],
      [ "pinb3", "lab2b_8py.html#a5cae82f0f114d3aba2d789635e8c0e2f", null ],
      [ "pinc13", "lab2b_8py.html#ab6f4be640dfe20a77e5f0edac3e99b0c", null ],
      [ "rTime", "lab2b_8py.html#a536dc98897ed4c3dccedbda3c0428b2d", null ],
      [ "timmy", "lab2b_8py.html#a6a7da999aefdca5b6c672c39de077d25", null ],
      [ "tries", "lab2b_8py.html#a75efbea126950aab12cec98fde20b2fa", null ]
    ] ],
    [ "vendo", null, [
      [ "kb_cb", "vendo_8py.html#a92b1e3a4a1d1398889f106f18cf46c6e", null ],
      [ "VendotronTask", "vendo_8py.html#a9fed49d43101743f5a8240f570f8727c", null ],
      [ "last_key", "vendo_8py.html#a10853866c82014f11d5621e87b52616e", null ],
      [ "vendo", "vendo_8py.html#afc7c9223cbbe0c449c296a7b3c5fbb99", null ]
    ] ]
];