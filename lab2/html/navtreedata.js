/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "ME405: Mechatronics", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Source Code Repository", "index.html#repo", null ],
    [ "HW 0x02: Term Project System Modeling", "hw0x02.html", null ],
    [ "Lab0x01: FSM Practice and User Interface Development", "lab0x01.html", [
      [ "Summary", "lab0x01.html#sec_lab0x01_summary", null ],
      [ "Design Features", "lab0x01.html#sec_lab0x01_features", null ],
      [ "User Inputs", "lab0x01.html#sec_lab0x01_inputs", null ],
      [ "Vendotron State Transition Diagram", "lab0x01.html#sec_lab0x01_diagram", null ],
      [ "Documentation", "lab0x01.html#lab0x01_documentation", null ],
      [ "Source Code", "lab0x01.html#lab0x01_source", null ]
    ] ],
    [ "Lab0x02: Interrupt Service Routines and Timing", "lab0x02.html", [
      [ "Summary", "lab0x02.html#sec_lab0x02_summary", null ],
      [ "Design Features - Part A", "lab0x02.html#sec_lab0x02_features_a", null ],
      [ "Design Features - Part B", "lab0x02.html#sec_lab0x02_features_b", null ],
      [ "Overview", "lab0x02.html#sec_lab0x02_overview", null ],
      [ "Documentation", "lab0x02.html#lab0x02_documentation", null ],
      [ "Source Code", "lab0x02.html#lab0x02_source", null ]
    ] ],
    [ "Packages", "namespaces.html", [
      [ "Packages", "namespaces.html", "namespaces_dup" ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"files.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';