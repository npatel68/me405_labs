## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website serves as documentation for code developed by Neil Patel in 
#  ME405: Mechatronics as well as his portfolio for the class. See individual
#  modules for details.\n\n
#  \b Modules:
#  - @ref lab0x01
#  - @ref lab0x02
#
#  @section repo Source Code Repository
#  The repository containing all the source code is:
#  https://bitbucket.org/npatel68/me405_labs/src/master/ \n
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 2, 2021
#
#  @page hw0x02 HW 0x02: Term Project System Modeling
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @image html HW0x02_pg_1.png "Page 1"
#  @image html HW0x02_pg_2.png "Page 2"
#  @image html HW0x02_pg_3.png "Page 3"
#  @image html HW0x02_pg_4.png "Page 4"
#  @image html HW0x02_pg_5.png "Page 5"
#  @image html HW0x02_pg_6.png "Page 6"
#
#  @page lab0x01 Lab0x01: FSM Practice and User Interface Development
#  @section sec_lab0x01_summary Summary
#  The objective of this assignment is to develop a a program that embodies the
#  function of the vending machine of assignment HW 0x00, where we created a
#  state transition diagram. State transition diagrams are high–level design 
#  tools that can be used to ensure that code will function according to stated
#  design requirements. The 
#  Vendotron runs cooperatively; the code does not contain any blocking 
#  commands.
#
#  @section sec_lab0x01_features Design Features
#  The Vendotron operates without utilizing any 'blocking' commands and hence,
#  functions cooperatively. The figure below depicts the machine that the
#  Finite State Machine has been modeled after.
#
#  @image html Vendotron.png "Figure 1: The Vendotron Machine (Source: Lab0x01 Handout)"
#  
#  The Vendotron has several buttons including one for each kind of drink that
#  is available. Additionally there is a small two-line LCD text display and
#  coin return button as you would expect on an average vending machine.\n
#
#  The Vendotron exhibits the following features:
#  - On startup, Vendotron displays an initialization message on the LCD 
#  screen.
#  - At any time a coin may be inserted, which results in the balance being
#  displayed on the screen.
#  - At any time, a drink may be selected. If the current balance is 
#  sufficient, the Vendotron vends the desired beverage through the flap at the 
#  bottom of the machine and then computes the correct resulting balance. If 
#  the current balance is insufficient to make the purchase then the machine 
#  displays an "Insufficient Funds" message and then displays the price for 
#  the selected item.
#  - At any time the Eject button may be pressed, in which case the machine 
#  returns the full balance through the coin return.
#  - The Vendotron keeps prompting the user to select another beverage if there 
#  is remaining balance.
#  - If the Vendotron is idle for a certain amount of time, it shows a 
#  "Try Cuke today!" scrolling message on the LCD display.\n
#
#  @section sec_lab0x01_inputs User Inputs
#  <b>Beverage Selection:</b>\n
#  'c' - Cuke\n
#  'p' - Popsi\n
#  's' - Spryte\n
#  'd' - Dr. Pupper\n\n
#
#  <b>Insert Payment</b>\n
#  '0' - Penny\n
#  '1' - Nickle\n
#  '2' - Dime\n
#  '3' - Quarter\n
#  '4' - $1\n
#  '5' - $5\n
#  '6' - $10\n
#  '7' - $20\n\n
#  
#  <b>Return Change</b>\n
#  'e' - Eject change\n\n
#
#  
#  @section sec_lab0x01_diagram Vendotron State Transition Diagram
#  @image html Vendotron_FSM.png "Figure 2: Vendotron State Transition Diagram"
#
#  @section lab0x01_documentation Documentation
#  Documentation of this lab can be found here: Lab0x01
#  @section lab0x01_source Source Code 
#  The source code for this lab: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab1/vendo.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date April 22, 2021
#
#  @page lab0x02 Lab0x02: Interrupt Service Routines and Timing
#  @section sec_lab0x02_summary Summary
#  The objective of this lab is to write some embedded code to develop a
#  program that responds almost instantaneously to an event using MicroPython.
#  We achieved this objective by designing a program that tests the reaction
#  time of a user in response to a light by pressing a button on the Nucleo. We
#  implemented interrupts to achieve this functionality. This assignment was
#  conducted in two parts to navigate the differences in the approaches and to
#  see which approach is more efficient.
#
#  @section sec_lab0x02_features_a Design Features - Part A
#  In the first approach, we implemented the program using the "utime" library.
#  The program waited for a random time between 2 and 3 seconds and the turned
#  on the green LED on the Nucleo. We recorded the time as soon as the LED
#  turned on. We set up an external interrupt on the blue "User" button so that
#  when the button is pressed we can again record the time and compute the
#  difference between the two times to get the reaction time and output it to
#  the terminal. The LED would remain On for only one second. When the program 
#  is stopped, the average reaction time is
#  calculated and displayed on the screen. If no attempts were made, an error
#  message is printed.
#
#  @section sec_lab0x02_features_b Design Features - Part B
#  In the second approach, instead of using the "utime" library, we used built-
#  in features like the timer modules of the STM32 MCU. We configured Timer 2
#  as a free-running counter and designed two callback functions to record
#  the times on two different channels:
#  
#  - The first callback function got triggered by an Output Compare (OC) 
#    compare match on PA5 using Channel 1 of Timer 2. This interrupt was
#    configured so that PA5 went high when the interrupt occurs. We used this
#    to schedule the timing of the green LED. This interrupt was also
#    responsible for turning the LED OFF after one second.
#
#  - The second callback function got triggered by by Input Capture (IC) on 
#    PB3 using Channel 2 of Timer 2. This interrupt was configured so that
#    the timer value was automatically stored in the timer channels capture
#    value whenever the interrupt was triggered. We used this time to compute
#    the reaction time of the user.\n\n
#
#  The overall flow of this approach was similar to the first one. It has the
#  same functionality when the keyboard interrupt is pressed as in the first
#  approach and the differnce in precision is neglible to the naked eye.
#
#  @section sec_lab0x02_overview Overview
#  Both approaches achieve the objective of determining the reaction time of
#  the user by incorporating features of the Nucleo. It is observed, however,
#  that utilizing the built-in hardware achieves greater precision while
#  handling the timing. Since this assignment's code is not too heavy on the
#  CPU, the difference in precision is neglible to our objective.
#
#
#  @section lab0x02_documentation Documentation
#  Documentation of this lab can be found here: Lab0x02
#  @section lab0x02_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2a.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2b.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 2, 2021
#