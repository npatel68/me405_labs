""" @file lab2b.py
    @brief This file contains a program which uses built in timer features of
           the STM32 MCU to testa a user's reaction time.
    @package Lab0x02
    @brief This package contains lab2a.py, lab2b.py
    @author Neil Patel
    @date May 2, 2021
"""

import pyb, micropython, random

## @brief The timer count used to keep track of the IC.
count = None
## @brief The timer count used to keep track of the OC.
comp = 0
## @brief Initialization of Timer2 as a free running counter.
timmy = pyb.Timer(2, prescaler = 8000, period = 0xffff)
timmy.counter()
## @brief Initialization of pin A5.
pina5 = pyb.Pin (pyb.Pin.board.PA5, pyb.Pin.OUT_PP)
## @brief Initialization of pin B3.
pinb3 = pyb.Pin (pyb.Pin.board.PB3, pyb.Pin.IN)
## @brief Initialization of pin C13.
pinc13 = pyb.Pin (pyb.Pin.board.PC13, pyb.Pin.IN)
## @brief Initialization of timer channel 1.
ch1 = timmy.channel(1, pyb.Timer.OC_TOGGLE, pin = pina5, 
                    polarity = pyb.Timer.HIGH)
## @brief Initialization of timer channel 2.
ch2 = timmy.channel(2, pyb.Timer.IC, pin = pinb3, polarity = pyb.Timer.FALLING)

def cb1(timer):
    """ @brief First Callback Function
        @details Triggered by an Output Compare (OC) compare match on PA5
                 using Channel 1 of Timer 2. PA5 goes high when interrupt
                 occurs and toggles otherwise.
        @param timer Timer the timer being used.
        @return Returns nothing.
    """
    global comp
    if pina5.value():
        comp += 10000
        pina5.low()
    else:
        comp += random.randrange(20000, 30000)
        pina5.high()
    comp = comp % 65535
    ch1.compare(comp)

ch1.callback(cb1)

def cb2(timer):
    """ @brief Second Callback Function
        @details Triggered by an Input Capture (IC) on PB3 using Channel 2 of
                 Timer 2. When IC interrupt is triggered, the timer value
                 is automatically stored in the capture value.
        @param timer Timer the timer being used.
        @return Returns nothing.
    """
    global count
    count = ch2.capture()

ch2.callback(cb2)

if __name__ == "__main__":
    
    ## Runs the program to test a user's reaction time.
    ## @ brief Keeps track of all the attempts.
    tries = []
    try:
        micropython.alloc_emergency_exception_buf(100)
        while True:
            if count is not None:
                ## @brief Computes the reaction time.
                rTime = (comp - count) % 65535
                print("Your reaction time was: ", (float)(rTime/10), "ms")
                count = None
    except KeyboardInterrupt:
        if not tries:
            print("No reaction time successfully measured.")
        else:
            print("Your average reaction time is: ", (float)(sum(tries)/len(tries)), "us")
        timmy.deinit
