var indexSectionsWithContent =
{
  0: "hklv",
  1: "l",
  2: "v",
  3: "kv",
  4: "lv",
  5: "hl"
};

var indexSectionNames =
{
  0: "all",
  1: "namespaces",
  2: "files",
  3: "functions",
  4: "variables",
  5: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Namespaces",
  2: "Files",
  3: "Functions",
  4: "Variables",
  5: "Pages"
};

