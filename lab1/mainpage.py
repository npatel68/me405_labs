## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website serves as documentation for code developed by Neil Patel in 
#  ME405: Mechatronics as well as his portfolio for the class. See individual
#  modules for details.\n\n
#  \b Modules:
#  - @ref lab0x01
#
#  @section repo Source Code Repository
#  The repository containing all the source code is:
#  https://bitbucket.org/npatel68/me405_labs/src/master/ \n
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date April 28, 2021
#
#  @page hw0x02 HW 0x02: Term Project System Modeling
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @image html HW0x02_pg_1.png "Page 1"
#  @image html HW0x02_pg_2.png "Page 2"
#  @image html HW0x02_pg_3.png "Page 3"
#  @image html HW0x02_pg_4.png "Page 4"
#  @image html HW0x02_pg_5.png "Page 5"
#  @image html HW0x02_pg_6.png "Page 6"
#
#  @page lab0x01 Lab 0x01: FSM Practice and User Interface Development
#  @section sec_lab0x01_summary Summary
#  The objective of this assignment is to develop a a program that embodies the
#  function of the vending machine of assignment HW 0x00, where we created a
#  state transition diagram. State transition diagrams are high–level design 
#  tools that can be used to ensure that code will function according to stated
#  design requirements. The 
#  Vendotron runs cooperatively; the code does not contain any blocking 
#  commands.
#
#  @section sec_lab0x01_features Design Features
#  The Vendotron operates without utilizing any 'blocking' commands and hence,
#  functions cooperatively. The figure below depicts the machine that the
#  Finite State Machine has been modeled after.
#
#  @image html Vendotron.png "Figure 1: The Vendotron Machine (Source: Lab0x01 Handout)"
#  
#  The Vendotron has several buttons including one for each kind of drink that
#  is available. Additionally there is a small two-line LCD text display and
#  coin return button as you would expect on an average vending machine.\n
#
#  The Vendotron exhibits the following features:
#  - On startup, Vendotron displays an initialization message on the LCD 
#  screen.
#  - At any time a coin may be inserted, which results in the balance being
#  displayed on the screen.
#  - At any time, a drink may be selected. If the current balance is 
#  sufficient, the Vendotron vends the desired beverage through the flap at the 
#  bottom of the machine and then computes the correct resulting balance. If 
#  the current balance is insufficient to make the purchase then the machine 
#  displays an "Insufficient Funds" message and then displays the price for 
#  the selected item.
#  - At any time the Eject button may be pressed, in which case the machine 
#  returns the full balance through the coin return.
#  - The Vendotron keeps prompting the user to select another beverage if there 
#  is remaining balance.
#  - If the Vendotron is idle for a certain amount of time, it shows a 
#  "Try Cuke today!" scrolling message on the LCD display.\n
#
#  @section sec_lab0x01_inputs User Inputs
#  <b>Beverage Selection:</b>\n
#  'c' - Cuke\n
#  'p' - Popsi\n
#  's' - Spryte\n
#  'd' - Dr. Pupper\n\n
#
#  <b>Insert Payment</b>\n
#  '0' - Penny\n
#  '1' - Nickle\n
#  '2' - Dime\n
#  '3' - Quarter\n
#  '4' - $1\n
#  '5' - $5\n
#  '6' - $10\n
#  '7' - $20\n\n
#  
#  <b>Return Change</b>\n
#  'e' - Eject change\n\n
#
#  
#  @section sec_lab0x01_diagram Vendotron State Transition Diagram
#  @image html Vendotron_FSM.png "Figure 2: Vendotron State Transition Diagram"
#
#  @section lab0x01_documentation Documentation
#  Documentation of this lab can be found here: Lab 0x01
#  @section lab0x01_source Source Code 
#  The source code for this lab: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab1/vendo.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date April 22, 2021
#