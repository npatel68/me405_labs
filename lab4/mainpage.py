## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website serves as documentation for code developed by Neil Patel in 
#  ME405: Mechatronics as well as his portfolio for the class. See individual
#  modules for details.\n\n
#  \b Modules:
#  - @ref lab0x01
#  - @ref lab0x02
#  - @ref lab0x03
#  - @ref lab0x04
#
#  @section repo Source Code Repository
#  The repository containing all the source code is:
#  https://bitbucket.org/npatel68/me405_labs/src/master/ \n
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 13, 2021
#
#  @page hw0x02 HW 0x02: Term Project System Modeling
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @image html HW0x02_pg_1.png "Page 1"
#  @image html HW0x02_pg_2.png "Page 2"
#  @image html HW0x02_pg_3.png "Page 3"
#  @image html HW0x02_pg_4.png "Page 4"
#  @image html HW0x02_pg_5.png "Page 5"
#  @image html HW0x02_pg_6.png "Page 6"
#
#  @page lab0x01 Lab0x01: FSM Practice and User Interface Development
#  @section sec_lab0x01_summary Summary
#  The objective of this assignment is to develop a a program that embodies the
#  function of the vending machine of assignment HW 0x00, where we created a
#  state transition diagram. State transition diagrams are high–level design 
#  tools that can be used to ensure that code will function according to stated
#  design requirements. The Vendotron runs cooperatively; the code does not
#  contain any blocking commands.
#
#  @section sec_lab0x01_features Design Features
#  The Vendotron operates without utilizing any 'blocking' commands and hence,
#  functions cooperatively. The figure below depicts the machine that the
#  Finite State Machine has been modeled after.
#
#  @image html Vendotron.png "Figure 1: The Vendotron Machine (Source: Lab0x01 Handout)"
#  
#  The Vendotron has several buttons including one for each kind of drink that
#  is available. Additionally there is a small two-line LCD text display and
#  coin return button as you would expect on an average vending machine.\n
#
#  The Vendotron exhibits the following features:
#  - On startup, Vendotron displays an initialization message on the LCD 
#  screen.
#  - At any time a coin may be inserted, which results in the balance being
#  displayed on the screen.
#  - At any time, a drink may be selected. If the current balance is 
#  sufficient, the Vendotron vends the desired beverage through the flap at the 
#  bottom of the machine and then computes the correct resulting balance. If 
#  the current balance is insufficient to make the purchase then the machine 
#  displays an "Insufficient Funds" message and then displays the price for 
#  the selected item.
#  - At any time the Eject button may be pressed, in which case the machine 
#  returns the full balance through the coin return.
#  - The Vendotron keeps prompting the user to select another beverage if there 
#  is remaining balance.
#  - If the Vendotron is idle for a certain amount of time, it shows a 
#  "Try Cuke today!" scrolling message on the LCD display.\n
#
#  @section sec_lab0x01_inputs User Inputs
#  <b>Beverage Selection:</b>\n
#  'c' - Cuke\n
#  'p' - Popsi\n
#  's' - Spryte\n
#  'd' - Dr. Pupper\n\n
#
#  <b>Insert Payment</b>\n
#  '0' - Penny\n
#  '1' - Nickle\n
#  '2' - Dime\n
#  '3' - Quarter\n
#  '4' - $1\n
#  '5' - $5\n
#  '6' - $10\n
#  '7' - $20\n\n
#  
#  <b>Return Change</b>\n
#  'e' - Eject change\n\n
#
#  
#  @section sec_lab0x01_diagram Vendotron State Transition Diagram
#  @image html Vendotron_FSM.png "Figure 2: Vendotron State Transition Diagram"
#
#  @section lab0x01_documentation Documentation
#  Documentation of this lab can be found here: Lab0x01
#  @section lab0x01_source Source Code 
#  The source code for this lab: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab1/vendo.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date April 22, 2021
#
#  @page lab0x02 Lab0x02: Interrupt Service Routines and Timing
#  @section sec_lab0x02_summary Summary
#  The objective of this lab is to write some embedded code to develop a
#  program that responds almost instantaneously to an event using MicroPython.
#  We achieved this objective by designing a program that tests the reaction
#  time of a user in response to a light by pressing a button on the Nucleo. We
#  implemented interrupts to achieve this functionality. This assignment was
#  conducted in two parts to navigate the differences in the approaches and to
#  see which approach is more efficient.
#
#  @section sec_lab0x02_features_a Design Features - Part A
#  In the first approach, we implemented the program using the "utime" library.
#  The program waited for a random time between 2 and 3 seconds and the turned
#  on the green LED on the Nucleo. We recorded the time as soon as the LED
#  turned on. We set up an external interrupt on the blue "User" button so that
#  when the button is pressed we can again record the time and compute the
#  difference between the two times to get the reaction time and output it to
#  the terminal. The LED would remain On for only one second. When the program 
#  is stopped, the average reaction time is
#  calculated and displayed on the screen. If no attempts were made, an error
#  message is printed.
#
#  @section sec_lab0x02_features_b Design Features - Part B
#  In the second approach, instead of using the "utime" library, we used built-
#  in features like the timer modules of the STM32 MCU. We configured Timer 2
#  as a free-running counter and designed two callback functions to record
#  the times on two different channels:
#  
#  - The first callback function got triggered by an Output Compare (OC) 
#    compare match on PA5 using Channel 1 of Timer 2. This interrupt was
#    configured so that PA5 went high when the interrupt occurs. We used this
#    to schedule the timing of the green LED. This interrupt was also
#    responsible for turning the LED OFF after one second.
#
#  - The second callback function got triggered by by Input Capture (IC) on 
#    PB3 using Channel 2 of Timer 2. This interrupt was configured so that
#    the timer value was automatically stored in the timer channels capture
#    value whenever the interrupt was triggered. We used this time to compute
#    the reaction time of the user.\n\n
#
#  The overall flow of this approach was similar to the first one. It has the
#  same functionality when the keyboard interrupt is pressed as in the first
#  approach and the differnce in precision is neglible to the naked eye.
#
#  @section sec_lab0x02_overview Overview
#  Both approaches achieve the objective of determining the reaction time of
#  the user by incorporating features of the Nucleo. It is observed, however,
#  that utilizing the built-in hardware achieves greater precision while
#  handling the timing. Since this assignment's code is not too heavy on the
#  CPU, the difference in precision is neglible to our objective.
#
#
#  @section lab0x02_documentation Documentation
#  Documentation of this lab can be found here: Lab0x02
#  @section lab0x02_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2a.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2b.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 2, 2021
#
#  @page lab0x03 Lab0x03: Pushing the Right Buttons
#  @section sec_lab0x03_summary Summary
#  The objective of this lab is to build a simple user interface to facilitate
#  communication between a user and laptop. Further, a digital device will be
#  used to measure analog inputs to examine and visualize how input voltage
#  changes as a function of time. We extended our use of interrupts to examine
#  the voltage response of the Nucleo™ to a button press.
#
#  @section sec_lab0x03_features_a Design Features - The Button Circuit
#  The User button on the Nucleo is connected to PC13, which does not have any
#  ADC input functionality. To combat this issue, we connect a jumper from PC13
#  to another pin that has ADC functionality (PA0 in this case). The change
#  made to the circuit is illustrated below:
#
#  @image html buttonCircuit.png "Figure 1: Initial Button Circuit" 
#
#  @image html moddedCircuit.png "Figure 2: Modified Button Circuit"
#
#  @section sec_lab0x03_features_b Design Features - Analog-to-Digital Conversion
#  We used an Analog-to-digital converter (ADC) to translate the continuous
#  analog voltage signal into discrete integers using a 3.3 V reference voltage.
#  We used the maximum 12-bit resolution to examine the board's voltage response.
#  Since the User button pin did not have any ADC functionality (as described in the
#  previous section), we had to jumper that pin to another pin designed
#  excclusively for Analog operations. After doing this, we were successfully
#  able to record the ADC readings of the voltage response and store it in a
#  buffer for further use.
#
#  @section sec_lab0x03_features_c Design Features - Serial Communication
#  To wrap everything together, we implemented Serial Communication using UART
#  to practically create a "virtual handshake" between the front end and the
#  Nucleo™.
#
#  @image html frontEnd.png "Figure 3: Serial Communication"
#
#  To set up the serial communciation properly, we first created a script that
#  would establish communiations with the Nucleo™. Then we created a script that
#  facilitated the software handshake between the board and the User Interface.
#  After that connection was succesfully made, we focused on capturing the
#  correct data from the ADC and successfully transmitting it over to the front
#  end. Once we successfully transferred the data, we used it to create the best fit
#  plot and a CSV file containing the data.
#
#  @section sec_lab0x03_overview Overview
#  We were successfully able to derive a plot of values that matched the
#  theoretical description. We accomplished this by finding the buffer in
#  which the greatest change took place by comparing the first and last value.
#  The plot obtained by doing so is displayed below.
#
#  @image html stepResponsePlot.png "Figure 4: ADC counts vs Time Plot"
#
#  Theoretically, the time constant RC = 0.5 ms. To get the time constant from
#  the plot, we found out the corresponding x value to y = 63.2% of VDD = 2588.04
#  ADC counts. The time constant using this method came out to be around 0.6 ms,
#  which is fairly close to our theoretical value. The equation and trendline
#  used to calculate this value is shown in the figure below. Notice that the
#  graph below does not consist data points when the ADC count is 0 to make the
#  process of determining time constant easier. Also notice, that the graph
#  has an x-intercept ofaround 1 ms, so that is also taken into consideration
#  when calculating the time constant.
#
#  @image html plotTrendline.png "Figure 5: Trendline Equation Plot"
#
#  @section lab0x03_documentation Documentation
#  Documentation of this lab can be found here: Lab0x03 \n
#  A copy of the CSV file of the good step response test can be found here: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/voltvstime.csv
#  
#  @section lab0x03_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/UI_front.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/main.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 6, 2021
#
#  @page lab0x04 Lab0x04: Hot or Not?
#  @section sec_lab0x04_summary Summary
#  The objective of this lab is to create and test a driver for an I2C-connected
#  sensor and use that sensor to log some data. The sensor in question is going
#  to be an MCP9808 temperature sensor and the data collected will be the
#  surrounding ambient temperature in an 8 hour window.
#
#  @section sec_lab0x04_features Design Features
#  The MCP9808 temperature sensor uses an I2C interface to communicate with the
#  MCU. In this assignment, the MCP can be utilized using only its power, ground,
#  and two I2C communication pins (SDA and SCL). A pin diagram for the MCU-MCP 
#  connection is shown below:
#  
#  @image html pinDiagram.png "Figure 1: Pin Diagram for I2C Connection"
#
#  This program contains a class that allows the user to communicate with the
#  MCP using its I2C interface. The class is able to initialize itself using
#  an I2C object and the MCP address on the I2C interface, verify that the sensor
#  is attached at the given bus address, and measure the temperature in degrees
#  Celcius and Fahrenheit. The program also measures the internal temperature
#  of the MCU and records both temperatures in degrees Celcius every 60 seconds.
#  Data is taken until the user presses "Ctrl+C" or till the time limit is
#  reached. Data is then saved in a CSV file and plotted.
#
#  @section sec_lab0x04_overview Overview
#  Since the sensor was placed in a room which was thermally regulated by a
#  thermostat, the temperature vs. time plot is nearly a straight line. Same
#  case with the MCU temperature readings as the temperature measured is internal
#  to the microcontroller. The y-axis is temperature in degrees Celcius and the
#  x-axis is time in seconds. The readings were taken in an 8-hour window from
#  2 pm to 10 pm. Series 1 is the internal temperature of the MCU (steady at 
#  rougly -5 degrees Celcius) and Series 2 is the readings from the temperature
#  sensor (steady at roughly 22 degrees Celcius). The plot is shown in the 
#  figure below:
#
#  @image html tempVsTime.png "Figure 2: Temperature vs. Time Plot"
#
#  @section lab0x04_documentation Documentation
#  Documentation of this lab can be found here: Lab0x04 \n
#  A copy of the CSV file can be found here:
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/temps.csv
#  
#  @section lab0x04_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/mcp9808.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/main.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 13, 2021
#