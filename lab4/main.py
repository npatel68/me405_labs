""" @file main.py
    @brief This file contains a program that takes temperature readings every
           60 seconds and saves them in a CSV file on the MCU.
    @package Lab0x04
    @brief This package contains mcp9808.py, main.py
    @author Neil Patel
    @date May 13, 2021
"""

from pyb import I2C
import pyb
from mcp9808 import iic
import utime

## @brief Implementation of  I2C serial communication
i2c = I2C(1)
## @brief Initialization of I2C in Master mode with baud rate 115200
i2c.init(I2C.MASTER, baudrate=115200)
## @brief Creating of I2C object that sets the Nucleo as the Master
sensor = iic(i2c, 0)
## @brief Initialization of ADC to 12-bit resolution on channels 16-18
adc = pyb.ADCAll(12, 0x70000)

try:
    if sensor.check():
        print("Press Ctrl-C to abort.")
        print("Recording data...")
        ## @brief Tracker of the universal start time
        start = utime.ticks_ms()
        ## @brief Tracker of the universal end time
        fin = start + (8 * 60 * 60 * 1000) # 8 hours
        with open ("temps.csv", "w") as file1:
            while utime.ticks_ms() < fin:
                ## @brief Time for the recorded entry in seconds
                rec_time = utime.ticks_diff(utime.ticks_ms(), start) / 1000
                ## @brief Internal temperature of the STM32 (in Celcius)
                temp1 = adc.read_core_temp()
                ## @brief Temperature recording from the sensor (in Celcius)
                temp2 = sensor.celcius()
                file1.write('{:},{:},{:}\r\n'.format(rec_time, temp1, temp2))
                utime.sleep(60) # wait 60 seconds before the next recording
        file1.close()
        print("Done recording.")

except KeyboardInterrupt():
    print('Aborted!')

i2c.deinit()