""" @file vendo.py
    @brief This file contains a Finite State Machine representaion of a 
           Vendotron vending machine.
    @package Lab0x01
    @brief This package contains vendo.py
    @author Neil Patel
    @date April 22, 2021
"""

from time import sleep
import keyboard

## @brief The input key pressed by the user.
last_key = ''

def kb_cb(key):
    """ @brief Callback function which is called when a key has been pressed.
        @param key String representation of the key pressed by the user.
        @return last_key String representation of the last key pressed by the 
                user, stored as a global variable.
    """
    global last_key
    last_key = key.name

# Tell the keyboard module to respond to these particular keys only
keyboard.on_release_key("0", callback=kb_cb)    # penny
keyboard.on_release_key("1", callback=kb_cb)    # nickel
keyboard.on_release_key("2", callback=kb_cb)    # dime
keyboard.on_release_key("3", callback=kb_cb)    # quarter
keyboard.on_release_key("4", callback=kb_cb)    # $1
keyboard.on_release_key("5", callback=kb_cb)    # $5
keyboard.on_release_key("6", callback=kb_cb)    # $10
keyboard.on_release_key("7", callback=kb_cb)    # $20
keyboard.on_release_key("e", callback=kb_cb)    # eject
keyboard.on_release_key("c", callback=kb_cb)    # cuke
keyboard.on_release_key("p", callback=kb_cb)    # popsi
keyboard.on_release_key("s", callback=kb_cb)    # spryte
keyboard.on_release_key("d", callback=kb_cb)    # dr. pupper

def VendotronTask():
    """ @brief Runs one iteration of the task.
        @details This program embodies the function of the vending machine
                 designed in HW 0x00. Performs a particular state's operations
                 in each iteration. On startup, display a message in the REPL
                 (state 0). Moves into state 1 and stays there where the user
                 can instruct the following actions -\n\n
                 1) Insert a coin by pressing one of the number keys (0-7) for
                    their respective currencies. On doing so, the balance will
                    be displayed in the REPL and the machine will go back to
                    state 1. The number keys are '0' for a penny, '1' for a
                    nickel, '2' for a dime, '3' for a quarter, '4' for $1, '5'
                    for $5, '6' for $10, and '7' for $20.\n\n
                 2) Select a drink by pressing one of the drink keys. If the
                    balance is insufficient, then an "Insufficient Funds"
                    message will be displayed on the REPL along with the
                    current balance and the price of the selected item and the
                    machine will go back to state 1. If the current balance is
                    sufficient, then the selcted item will be vended and the
                    current balance computed. If there is no remaining balance
                    left, then the machine will go back to state 1. However, if
                    there is any remaining balance, then the user will be
                    prompted to select another beverage. The drink keys are 'c'
                    for Cuke, 'p' for Popsi, 's' for Spryte, 'd' for Dr. Pupper.\n\n
                 3) Eject at any time by pressing the 'e' key. On doing so, the
                    full balance will be returned and the machine will go back
                    to state 1.\n\n
                 If the machine is in state 1 for a long time, then it will go
                 into an 'Idle' state in which a message will be displayed in
                 the REPL saying 'Try Cuke today!'.
        @return Does not return anything. Yields the state at the end so that
                the appropriate state is operated in the next iteration.
    """
    ## @brief Keeps track of which state the machine is in.
    state = 0
    ## @brief Keeps track of the user's balance (in cents).
    balance = 0         # in cents
    ## @brief Keeps track of the price of the drink selected.
    drink_price = 0     # in cents
    ## @brief Keeps track of the last key pressed by the user (global variable).
    global last_key
    ## @brief Keeps track of the name of the drink selected.
    drink = ''
    
    while True:
        # Implement FSM using a while loop and an if statement
        # will run eternally until user presses CTRL-C
        
        if state == 0:
            # perform state 0 operations (INIT)
            # this init state, initializes the FSM itself, with all the
            # code features already set up
            # Displays startup message
            print("Welcome to the Vendotron!")
            state = 1       # on the next iteration, the FSM will run state 1
            
        elif state == 1:
            idle_count = 0
            # perform state 1 operations (WAIT)
            # waits for a key press and sends into idle state if inactive for
            # some time
            while True:
                if last_key == '0':
                    last_key = ''
                    balance += 1
                    state = 2
                elif last_key == '1':
                    last_key = ''
                    balance += 5
                    state = 2
                elif last_key == '2':
                    last_key = ''
                    balance += 10
                    state = 2
                elif last_key == '3':
                    last_key = ''
                    balance += 25
                    state = 2
                elif last_key == '4':
                    last_key = ''
                    balance += 100
                    state = 2
                elif last_key == '5':
                    last_key = ''
                    balance += 500
                    state = 2
                elif last_key == '6':
                    last_key = ''
                    balance += 1000
                    state = 2
                elif last_key == '7':
                    last_key = ''
                    balance += 2000
                    state = 2
                elif last_key == 'e':
                    last_key = ''
                    state = 6
                elif last_key == 'c':
                    last_key = ''
                    drink = 'Cuke'
                    state = 3
                elif last_key == 'p':
                    last_key = ''
                    drink = 'Popsi'
                    state = 3
                elif last_key == 's':
                    last_key = ''
                    drink = 'Spryte'
                    state = 3
                elif last_key == 'd':
                    last_key = ''
                    drink = 'Dr. Pupper'
                    state = 3
                elif idle_count > 5000000:
                    state = 7
                else:
                    idle_count += 1
                if state != 1:
                    break;
        
        elif state == 2:
            # perform state 2 operations (BALANCE)
            # displays the current balance of the user whenever a coin is
            # inserted
            print("Your balance is: $", (float)(balance/100))
            state = 1   #s2 -> s1

        elif state == 3:
            # perform state 3 operations (DRINK)
            # computes the prink of the selected drink and determines if the
            # current balance is sufficient or not
            if drink == 'Cuke':
                drink_price = 100
            elif drink == 'Popsi':
                drink_price = 120
            elif drink == 'Spryte':
                drink_price = 85
            elif drink == 'Dr. Pupper':
                drink_price = 110
            if balance < drink_price:
                state = 4   #s3 -> 4
            else:
                print('Vended ', drink)
                balance -= drink_price
                state = 5   #s3 -> 5

        elif state == 4:
            # perform state 4 operations (INSUF)
            # displays message saying the balance is insufficient, along with
            # the current balnace and the price of the selcted item
            print("Insufficient Funds")
            print("Your balance: $", (float)(balance/100))
            print("Price of selected item: $", (float)(drink_price/100))
            state = 1   #s4 -> s1
            
        elif state == 5:
            # perform state 5 operations (SUF)
            # vends desired beverage and prompts user to select another
            # beverage if there is any remaining balance
            if balance != 0:
                print("Select another beverage!")
                print("Remaining balance: $", (float)(balance/100))
            state = 1   #s5 -> s1
            
        elif state == 6:
            # perform state 6 operations (EJECT)
            # returns full balance
            print('Returned balance: $', (float)(balance/100))
            balance = 0
            state = 0   #s6 -> s0
            
        elif state == 7:
            # perform state 7 operations (IDLE)
            # displays message saying "Try Cuke today!" if machine left idle
            # for some time
            print("Try Cuke today!")
            state = 1   #s7 -> s1

        yield(state)



if __name__ == "__main__":
    
    ## Initialize code to run FSM (not the init state). This means building 
    ## the iterator as generator object

    vendo = VendotronTask()
    
    # Run the task every 10ms (approximately) unless we get a ctrl-c or the
    # task stops yielding.
    
    try:
        while True:
            next(vendo)
            sleep(0.01)
            
    except KeyboardInterrupt:
        print('Ctrl-c detected. Goodbye')
        keyboard.unhook_all ()
            
    except StopIteration:
        print('Task has stopped yielding its state. Check your code.')