""" @file mcp9808.py
    @brief This file contains a class that allows a user to communicate with an
           MCP9808 temperature sensor using its I2C interface.
    @package Lab0x04
    @brief This package contains mcp9808.py, main.py
    @author Neil Patel
    @date May 13, 2021
"""

from pyb import I2C
import pyb

class iic:
    """ @brief Class that allows the user to communicate with the MCP9808
               temperature sensor using its I2C interface.
        @details This class creates a MCP object that interfaces with the I2C
                 object. The initializer is given a reference to an already
                 created I2C object and the address of the MCP9808 on the I2C
                 bus. This class also contains methods to veriy the sensor,
                 and take temperature readings in Celcius and Fahrenheit.
    """
    
    def __init__(self, i2c, addr):
        """ @brief Initializer for the class.
            @details Initializes the class using the reference to the I2C 
                     object and the MCP9808 address.
            @param i2c I2C The I2C Serial Communication object.
            @param addr int The address of the MCP9808
            @return Returns nothing.
        """
        self.sens = i2c
        self.address = 0b11000 + addr # added to the default adress of 24 (0x18)
        
    def check(self):
        """ @brief Verifies the sensor connection.
            @details Verifies that the sensor is attached at the given bus
                     address by checking the returned manufacture ID.
            @return Returns a boolean verifying the sensor connection.
        """
        ## @brief A buffer to hold 2 bytes of data from the I2C.
        buffy = bytearray(2)
        self.sens.mem_read(buffy, self.address, 6)
        return 0x0054 == buffy[1] # 0x0054 is the manufacture ID.
        
    def celcius(self):
        """ @brief Measures the temperature in degrees Celcius.
            @return Returns a float that contains the measured temperature in
                    degrees Celcius.
        """
        ## @brief A buffer to hold 2 bytes of data from the I2C.
        buffy = bytearray(2)
        self.sens.mem_read(buffy, self.address, 5)
        ## @brief Variable holding the measured temperature value.
        temp = 0
        ## @brief READ 8 bits and send ACK bit.
        upper = buffy[0]
        ## @brief READ 8 bits and send NAK bit.
        lower = buffy[1]
        upper &= 0x1F # Clear flag bits
        if ((upper & 0x10) == 0x10): # T < 0
            upper &= 0x0F # Clear SIGN
            temp = 256.0 - (upper * 16.0 + lower / 16.0)
        else:
            temp = (upper * 16.0 + lower / 16.0)
        return temp
    
    def fahrenheit(self):
        """ @brief Measures the temperature in degrees Fahrenheit.
            @return Returns a float that contains the measured temperature in
                    degrees Fahrenheit.
        """
        ## @brief Variable holding the measured temperature value in degrees Fahrenheit.
        temp = (self.celcius() * 9.0/5.0) + 32.0
        return temp

if __name__ == '__main__':
    ## @brief Implementation of  I2C serial communication    
    i2c = I2C(1)
    ## @brief Initialization of I2C in Master mode with baud rate 115200
    i2c.init(I2C.MASTER, baudrate=115200)
    ## @brief Creating of I2C object that sets the Nucleo as the Master
#    i2c.deinit()

    eye2see = iic(i2c, 0)
    try:
        while eye2see.check():
            print("Temperature is " + str(eye2see.celcius()) + " degrees Celcius or " 
                  + str(eye2see.fahrenheit()) + " degrees Fahrenheit.")
            pyb.delay(1000)
    except KeyboardInterrupt():
        print('Aborted!')
    
    i2c.deinit()
