## @file mainpage.py
#  Documentation for / use of mainpage.py
#
#  Detailed doc for mainpage.py 
#
#  @mainpage
#
#  @section sec_intro Introduction
#  This website serves as documentation for code developed by Neil Patel in 
#  ME405: Mechatronics as well as his portfolio for the class. See individual
#  modules for details.\n\n
#  \b Modules:
#  - @ref hw0x02
#  - @ref hw0x04
#  - @ref hw0x05
#  - @ref lab0x01
#  - @ref lab0x02
#  - @ref lab0x03
#  - @ref lab0x04
#
#  @section repo Source Code Repository
#  The repository containing all the source code is:
#  https://bitbucket.org/npatel68/me405_labs/src/master/ \n
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 16, 2021
#
#  @page hw0x02 HW0x02: Term Project System Modeling
#  @image html HW0x02_pg_1.png "Page 1"
#  @image html HW0x02_pg_2.png "Page 2"
#  @image html HW0x02_pg_3.png "Page 3"
#  @image html HW0x02_pg_4.png "Page 4"
#  @image html HW0x02_pg_5.png "Page 5"
#  @image html HW0x02_pg_6.png "Page 6"
#  - - -
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @copyright Copyright © 2021 Neil Patel
#  @date April 30, 2021
#
#  @page hw0x04 HW0x04: Simulation or Reality?
#  @section sec_hw0x04_overview Overview
#  The goal of this assignment is to model the Equations of Motion derived in 
#  HW0x02 as a Linear State Space in the form of x_dot = Ax + Bu, where x is 
#  the state vector, A is the coefficient Matrix to the state vector, u is the 
#  input vector, B is the coeffcient Matrix to the input vector, and x_dot is 
#  the time derivative of the state vector x. In the context of the Equations 
#  of Motion representing the Ball-Plate system derived in Homework 2, the 
#  state vector would be a row vector of the position x, angle theta y and their 
#  derivatives, while the input vector is the torque output from the motor, and 
#  x_dot is the acceleration of the ball and the angular acceleration of the 
#  platform about the y axis. The implementation of this state space involved 
#  getting a matrix G that represented the matrix of accelerations from the 
#  inverse of the Mass Matrix M times the Forcing Function Matrix f. From there, 
#  the G matix was appended to a matrix h with x_dot (velocity) and theta_dot 
#  (angular velocity) as the first two elements in that matrix. The model was 
#  then linearized using Jacobian Matricies, which yielded A and B. From there, 
#  the Matricies A and B were implemented into a MatLab Simulink Model (see below) 
#  that calculated the output (position, angle, velocity and angular velocity) 
#  for both Open and Closed-loop controller set ups. Note the Gain for open loop 
#  is given as K = [0 0 0 0], and the Gain for closed loop was approximated to 
#  be K = [-0.3 -0.2 -0.05 -0.02] in the interest of evaluating system performance. 
#  Three test cases were used to verify the model's accuracy:
#
#  - Open Loop, with all 0 initial conditions, running for 1 second.
#  - Open Loop, with the only non-zero initial condition being x = 5 cm, running 
#  for 0.4 seconds.
#  - Closed Loop, with the same initial conditions as Case 2 except T_x = -Kx, 
#  running for 20 seconds.\n\n
#
#  @image html simLink.png "The Simulink Model used to generate output."
#
#  @section sec_hw0x04_case1 Case 1: Open Loop with Zero Initial Conditions
#
#  @image html case1_1.png
#  @image html case1_2.png
#
#  As one might expect from a system with zero initial conditions, there is no 
#  response in any element of the state vector. This acts as a sort of first 
#  "sanity check" that ensures no zero error occurs with no input in the model.
#
#  @section sec_hw0x04_case2 Case 2: Open Loop with x_i = 5 cm
#
#  @image html case2_1.png
#  @image html case2_2.png
#
#  This acts as a second sanity check, where the ball starts 5 cm off the center 
#  of gravity of the platform with no torque input from the motor. The results 
#  are in agreement with what one would expect - that the platform begins to 
#  tilt due to the moment caused by the weight of the ball about the platform's 
#  center of rotation. The ball is shown to move towards the center a bit before 
#  rolling to the edge of the platform at an increasing velocity as the angle of 
#  platform tilt increases. With no torque input to correct this, the ball will 
#  simply roll off the edge at some time t in the future. Moreover, the ball moves 
#  barely more than a centimeter and reaches a velocity of just over 0.2 m/s, 
#  which makes sense given the short time frame and beginnng from rest - this 
#  validates the model for open loop responses from an intuition point of view.
#
#  @section sec_hw0x04_case3 Case 3: Closed Loop with x_i = 5 cm and T_x = -Kx
#
#  @image html case3_1.png
#  @image html case3_2.png
#
#  Finally, we have a closed control loop with torque input from the motor and 
#  constant gain values. Note the gain values used are by no means ideal, which 
#  is why the system response takes roughly 15 to 20 seconds to reach steady state 
#  that is, the ball is balanced in the center of the platform. Otherwise, the 
#  output on the plots mostly agree with what makes sense with intuition. The 
#  ball oscillates from side to side on the platform as the platform moves to 
#  decrease the velocity of the ball and bring it back to a neutral position. 
#  As expected, the plots show a gradual decay in the magnitude of all state 
#  variables until they reach a steady state as the ball approaches the center 
#  of platform. Additionally, note the magnitude of the numbers makes sense, 
#  since the ball's position always decreases from 5 cm, while the platform's 
#  angle initially increases with every peak to compensate for the ball's rolling, 
#  but it eventually decreases as the ball's rolling becomes more controlled. 
#  Additionally, the ball's velocity never grows much beyond 0.1 m/s and the 
#  platform's angular velocity never gets larger than 0.2 seconds. These low 
#  numbers make sense for a system this small in scale, which validates the 
#  model's efficacy for Closed Loop Control. 
#
#  @section hw0x04_source Source Code 
#  For the live Matlab script and Simulink Model, please visit this link: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/hw4/
#  - - -
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @copyright Copyright © 2021 Neil Patel
#  @date May 9, 2021
#
#  @page hw0x05 HW0x05: Full State Feedback
#  @section sec_hw0x05_overview Overview
#  This assignment extends the model developed for HW0x04 to derive the controller 
#  gain values for the ball-plate system. There were four new sections added:
#  - Deriving the K values symbolically from a 4th order generic characteristic 
#  polynomial for a closed loop system.
#  - Finding the pole locations using 2 arbitrarily chosen pole locations (negative), 
#  an arbitrary natural frequency of the system, and an arbitrary damping ratio, 
#  for which another 4th order generic characteristic polynomial is created.
#  - Calculating the K values by equating the polynomials from the prior 2 steps, 
#  and solving for the gain values. Additionally, the calculated gain values are 
#  double checked using Matlab's Control System toolbox.
#  - Finally, the system response with the new gain values is plotted.
#
#  @section sec_hw0x05_res Simulation Results and Performance
#  @image html res1.png
#  @image html res2.png "The closed loop results from HW0x04, for comparison with the tuned system response from HW0x05"
#  @image html res3.png
#  @image html res4.png "The tuned closed loop response for the following gain values: K = [-5.7941 -1.5335 -1.5612 -0.1296]"
#
#  Note the significantly lower timescale of the response from the calculated k 
#  values (~0.6s to steady state) when compared to the response from the given 
#  k values (~15s to steady state)- this is much more ideal for the platform 
#  because the ball will be balanced in about 3 "tilts" rather than almost 20 
#  "tilts." This will reduce the load on the motors when balancing the ball, and 
#  will allow the system to respond dynamically to various new stimuli, like a 
#  hand moving the ball after balancing, or the platform getting hit, causing 
#  the ball to move. Additionally, the maximum angle of tilt for the calcualted 
#  k values (~11 degrees), while greater than the maximum angle of tilt for the 
#  given k values by about 3 times, is still reasonably small such that the linear 
#  state space model remains accurate. Additionally, the angular velocity of the 
#  platform, while much greater for the calculated k values, is still low enough 
#  (in magnitude) at maxima and minima that the platform should be able to handle 
#  these speeds. 
#
#  @section hw0x05_source Source Code 
#  For more information on the implementation of this process as well as the 
#  simulink model and foundational Matlab code used in this assignment, please 
#  visit this link: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/hw5/
#  - - -
#  @author Group 2: Neil Patel, Jacob Burghgraef
#  @copyright Copyright © 2021 Neil Patel
#  @date May 16, 2021
#
#  @page lab0x01 Lab0x01: FSM Practice and User Interface Development
#  @section sec_lab0x01_summary Summary
#  The objective of this assignment is to develop a a program that embodies the
#  function of the vending machine of assignment HW 0x00, where we created a
#  state transition diagram. State transition diagrams are high–level design 
#  tools that can be used to ensure that code will function according to stated
#  design requirements. The Vendotron runs cooperatively; the code does not
#  contain any blocking commands.
#
#  @section sec_lab0x01_features Design Features
#  The Vendotron operates without utilizing any 'blocking' commands and hence,
#  functions cooperatively. The figure below depicts the machine that the
#  Finite State Machine has been modeled after.
#
#  @image html Vendotron.png "Figure 1: The Vendotron Machine (Source: Lab0x01 Handout)"
#  
#  The Vendotron has several buttons including one for each kind of drink that
#  is available. Additionally there is a small two-line LCD text display and
#  coin return button as you would expect on an average vending machine.\n
#
#  The Vendotron exhibits the following features:
#  - On startup, Vendotron displays an initialization message on the LCD 
#  screen.
#  - At any time a coin may be inserted, which results in the balance being
#  displayed on the screen.
#  - At any time, a drink may be selected. If the current balance is 
#  sufficient, the Vendotron vends the desired beverage through the flap at the 
#  bottom of the machine and then computes the correct resulting balance. If 
#  the current balance is insufficient to make the purchase then the machine 
#  displays an "Insufficient Funds" message and then displays the price for 
#  the selected item.
#  - At any time the Eject button may be pressed, in which case the machine 
#  returns the full balance through the coin return.
#  - The Vendotron keeps prompting the user to select another beverage if there 
#  is remaining balance.
#  - If the Vendotron is idle for a certain amount of time, it shows a 
#  "Try Cuke today!" scrolling message on the LCD display.\n
#
#  @section sec_lab0x01_inputs User Inputs
#  <b>Beverage Selection:</b>\n
#  'c' - Cuke\n
#  'p' - Popsi\n
#  's' - Spryte\n
#  'd' - Dr. Pupper\n\n
#
#  <b>Insert Payment</b>\n
#  '0' - Penny\n
#  '1' - Nickle\n
#  '2' - Dime\n
#  '3' - Quarter\n
#  '4' - $1\n
#  '5' - $5\n
#  '6' - $10\n
#  '7' - $20\n\n
#  
#  <b>Return Change</b>\n
#  'e' - Eject change\n\n
#
#  
#  @section sec_lab0x01_diagram Vendotron State Transition Diagram
#  @image html Vendotron_FSM.png "Figure 2: Vendotron State Transition Diagram"
#
#  @section lab0x01_documentation Documentation
#  Documentation of this lab can be found here: Lab0x01
#  @section lab0x01_source Source Code 
#  The source code for this lab: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab1/vendo.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date April 22, 2021
#
#  @page lab0x02 Lab0x02: Interrupt Service Routines and Timing
#  @section sec_lab0x02_summary Summary
#  The objective of this lab is to write some embedded code to develop a
#  program that responds almost instantaneously to an event using MicroPython.
#  We achieved this objective by designing a program that tests the reaction
#  time of a user in response to a light by pressing a button on the Nucleo. We
#  implemented interrupts to achieve this functionality. This assignment was
#  conducted in two parts to navigate the differences in the approaches and to
#  see which approach is more efficient.
#
#  @section sec_lab0x02_features_a Design Features - Part A
#  In the first approach, we implemented the program using the "utime" library.
#  The program waited for a random time between 2 and 3 seconds and the turned
#  on the green LED on the Nucleo. We recorded the time as soon as the LED
#  turned on. We set up an external interrupt on the blue "User" button so that
#  when the button is pressed we can again record the time and compute the
#  difference between the two times to get the reaction time and output it to
#  the terminal. The LED would remain On for only one second. When the program 
#  is stopped, the average reaction time is
#  calculated and displayed on the screen. If no attempts were made, an error
#  message is printed.
#
#  @section sec_lab0x02_features_b Design Features - Part B
#  In the second approach, instead of using the "utime" library, we used built-
#  in features like the timer modules of the STM32 MCU. We configured Timer 2
#  as a free-running counter and designed two callback functions to record
#  the times on two different channels:
#  
#  - The first callback function got triggered by an Output Compare (OC) 
#    compare match on PA5 using Channel 1 of Timer 2. This interrupt was
#    configured so that PA5 went high when the interrupt occurs. We used this
#    to schedule the timing of the green LED. This interrupt was also
#    responsible for turning the LED OFF after one second.
#
#  - The second callback function got triggered by by Input Capture (IC) on 
#    PB3 using Channel 2 of Timer 2. This interrupt was configured so that
#    the timer value was automatically stored in the timer channels capture
#    value whenever the interrupt was triggered. We used this time to compute
#    the reaction time of the user.\n\n
#
#  The overall flow of this approach was similar to the first one. It has the
#  same functionality when the keyboard interrupt is pressed as in the first
#  approach and the differnce in precision is neglible to the naked eye.
#
#  @section sec_lab0x02_overview Overview
#  Both approaches achieve the objective of determining the reaction time of
#  the user by incorporating features of the Nucleo. It is observed, however,
#  that utilizing the built-in hardware achieves greater precision while
#  handling the timing. Since this assignment's code is not too heavy on the
#  CPU, the difference in precision is neglible to our objective.
#
#
#  @section lab0x02_documentation Documentation
#  Documentation of this lab can be found here: Lab0x02
#  @section lab0x02_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2a.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab2/lab2b.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 2, 2021
#
#  @page lab0x03 Lab0x03: Pushing the Right Buttons
#  @section sec_lab0x03_summary Summary
#  The objective of this lab is to build a simple user interface to facilitate
#  communication between a user and laptop. Further, a digital device will be
#  used to measure analog inputs to examine and visualize how input voltage
#  changes as a function of time. We extended our use of interrupts to examine
#  the voltage response of the Nucleo™ to a button press.
#
#  @section sec_lab0x03_features_a Design Features - The Button Circuit
#  The User button on the Nucleo is connected to PC13, which does not have any
#  ADC input functionality. To combat this issue, we connect a jumper from PC13
#  to another pin that has ADC functionality (PA0 in this case). The change
#  made to the circuit is illustrated below:
#
#  @image html buttonCircuit.png "Figure 1: Initial Button Circuit" 
#
#  @image html moddedCircuit.png "Figure 2: Modified Button Circuit"
#
#  @section sec_lab0x03_features_b Design Features - Analog-to-Digital Conversion
#  We used an Analog-to-digital converter (ADC) to translate the continuous
#  analog voltage signal into discrete integers using a 3.3 V reference voltage.
#  We used the maximum 12-bit resolution to examine the board's voltage response.
#  Since the User button pin did not have any ADC functionality (as described in the
#  previous section), we had to jumper that pin to another pin designed
#  excclusively for Analog operations. After doing this, we were successfully
#  able to record the ADC readings of the voltage response and store it in a
#  buffer for further use.
#
#  @section sec_lab0x03_features_c Design Features - Serial Communication
#  To wrap everything together, we implemented Serial Communication using UART
#  to practically create a "virtual handshake" between the front end and the
#  Nucleo™.
#
#  @image html frontEnd.png "Figure 3: Serial Communication"
#
#  To set up the serial communciation properly, we first created a script that
#  would establish communiations with the Nucleo™. Then we created a script that
#  facilitated the software handshake between the board and the User Interface.
#  After that connection was succesfully made, we focused on capturing the
#  correct data from the ADC and successfully transmitting it over to the front
#  end. Once we successfully transferred the data, we used it to create the best fit
#  plot and a CSV file containing the data.
#
#  @section sec_lab0x03_overview Overview
#  We were successfully able to derive a plot of values that matched the
#  theoretical description. We accomplished this by finding the buffer in
#  which the greatest change took place by comparing the first and last value.
#  The plot obtained by doing so is displayed below.
#
#  @image html stepResponsePlot.png "Figure 4: ADC counts vs Time Plot"
#
#  Theoretically, the time constant RC = 0.5 ms. To get the time constant from
#  the plot, we found out the corresponding x value to y = 63.2% of VDD = 2588.04
#  ADC counts. The time constant using this method came out to be around 0.6 ms,
#  which is fairly close to our theoretical value. The equation and trendline
#  used to calculate this value is shown in the figure below. Notice that the
#  graph below does not consist data points when the ADC count is 0 to make the
#  process of determining time constant easier. Also notice, that the graph
#  has an x-intercept ofaround 1 ms, so that is also taken into consideration
#  when calculating the time constant.
#
#  @image html plotTrendline.png "Figure 5: Trendline Equation Plot"
#
#  @section lab0x03_documentation Documentation
#  Documentation of this lab can be found here: Lab0x03 \n
#  A copy of the CSV file of the good step response test can be found here: 
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/voltvstime.csv
#  
#  @section lab0x03_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/UI_front.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab3/main.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 6, 2021
#
#  @page lab0x04 Lab0x04: Hot or Not?
#  @section sec_lab0x04_summary Summary
#  The objective of this lab is to create and test a driver for an I2C-connected
#  sensor and use that sensor to log some data. The sensor in question is going
#  to be an MCP9808 temperature sensor and the data collected will be the
#  surrounding ambient temperature in an 8 hour window.
#
#  @section sec_lab0x04_features Design Features
#  The MCP9808 temperature sensor uses an I2C interface to communicate with the
#  MCU. In this assignment, the MCP can be utilized using only its power, ground,
#  and two I2C communication pins (SDA and SCL). A pin diagram for the MCU-MCP 
#  connection is shown below:
#  
#  @image html pinDiagram.png "Figure 1: Pin Diagram for I2C Connection"
#
#  This program contains a class that allows the user to communicate with the
#  MCP using its I2C interface. The class is able to initialize itself using
#  an I2C object and the MCP address on the I2C interface, verify that the sensor
#  is attached at the given bus address, and measure the temperature in degrees
#  Celcius and Fahrenheit. The program also measures the internal temperature
#  of the MCU and records both temperatures in degrees Celcius every 60 seconds.
#  Data is taken until the user presses "Ctrl+C" or till the time limit is
#  reached. Data is then saved in a CSV file and plotted.
#
#  @section sec_lab0x04_overview Overview
#  Since the sensor was placed in a room which was thermally regulated by a
#  thermostat, the temperature vs. time plot is nearly a straight line. Same
#  case with the MCU temperature readings as the temperature measured is internal
#  to the microcontroller. The y-axis is temperature in degrees Celcius and the
#  x-axis is time in seconds. The readings were taken in an 8-hour window from
#  2 pm to 10 pm. Series 1 is the internal temperature of the MCU (steady at 
#  rougly -5 degrees Celcius) and Series 2 is the readings from the temperature
#  sensor (steady at roughly 22 degrees Celcius). The plot is shown in the 
#  figure below:
#
#  @image html tempVsTime.png "Figure 2: Temperature vs. Time Plot"
#
#  @section lab0x04_documentation Documentation
#  Documentation of this lab can be found here: Lab0x04 \n
#  A copy of the CSV file can be found here:
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/temps.csv
#  
#  @section lab0x04_source Source Code 
#  The source code for this lab: \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/mcp9808.py \n
#  https://bitbucket.org/npatel68/me405_labs/src/master/lab4/main.py
#  - - -
#  @author Neil Patel
#  @copyright Copyright © 2021 Neil Patel
#  @date May 13, 2021
#