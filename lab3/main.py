""" @file main.py
    @brief This file contains a program to facilitate the data collection task
           from the Nucleo™ and the "software handshake".
    @details Data collection task begins upon receipt of a specific character
             from the serial. The data collection task involves the sampling
             from the Blue User button on the Nucleo. When a good step response
             is measured, the data is evaluated through the ADC and sent back
             to the PC in a batch.
    @package Lab0x03
    @brief This package contains UI_front.py, main.py
    @author Neil Patel
    @date May 6, 2021
"""
from pyb import UART
import array
import pyb

## @brief Creates the UART instance.
myuart = UART (2)
## @brief Initiallizes interrupt on the User button.
pinc13 = pyb.Pin(pyb.Pin.board.PC13, pyb.Pin.IN)
## @brief Initiallizes ADC on one of the pins.
pina0 = pyb.Pin(pyb.Pin.cpu.A0, pyb.Pin.IN)
adc = pyb.ADC(pina0)
## @brief Buffer array for capture values.
buffy = array.array('H', (0 for index in range(200)))        

while True:
    # Start recording data if the character is received and the button is
    # pressed.
    if myuart.any() != 0 and pinc13.value() == 0:
        ## @brief Keeps track of the timestamps.
        time = 0
        ## @brief Calculated period of each step.
        period = 25 # us
        while True:
            adc.read_timed(buffy, 40000)
            # Condition for a good step response
            if buffy[-1] - buffy[0] >= 4050:
                for n in range(len(buffy)):
                    print(time, buffy[n])
                    time += period
                break
        break
